<?php

// change directory to root.
chdir(dirname(__DIR__));

require_once 'app/Bootstrap.php';

$bootstrap = new Bootstrap();
$bootstrap->run();

