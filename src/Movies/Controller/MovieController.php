<?php

namespace Movies\Controller;

use Framework\Slim\Controller\AbstractController;
use Framework\Slim\Http\Response;
use Framework\Slim\Http\JsonResponse;
use Framework\Slim\Http\HttpResponse;

use Movies\Models\Mongodb\Movie;
use Movies\Helper\Pager;

class MovieController extends AbstractController
{
    /**
     * The maximum number of movies to show on a page.
     *
     * @var int
     */
    const MAX_RESULTS = 25;

    /**
     * MongoDB document manager.
     *
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * Initialize controller.
     */
    public function init()
    {
        $this->documentManager = $this->getServiceLocator()->get('DocumentManager');
    }

    /**
     * Display a collection of movies.
     *
     * @return JsonResponse a JSON response.
     */
    public function getAll()
    {
        // limit and skip to paginate.
        $limit  = $this->getLimit();
        $skip   = $this->getSkip();
        // sort and ordering of results.
        $orderby = $this->getOrderBy();
        $order   = $this->getOrder();

        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Movie');
        $results = $queryBuilder->limit($limit)
                                ->skip($skip)
                                ->sort($orderby, $order)
                                ->getQuery()
                                ->execute();

        if ($results->count() == 0) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        try {        
            return new JsonResponse($this->createMovies($results));
        } catch (\InvalidArgumentException $e) {
            return new HttpResponse(null, Response::HTTP_INTERNAL_ERROR);
        }
    }
    
    /**
     * Displays a movie that matches with the given id.
     *
     * @param string $id the id of the movie.
     * @return JsonResponse a JSON response.
     */
    public function get($id)
    {
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Movie');
        $result = $queryBuilder->field('id')->equals($id)
                               ->getQuery()
                               ->getSingleResult();

        if (!$result) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        try {
            return new JsonResponse($this->createMovie($result));
        } catch (\InvalidArgumentException $e) {
            return new HttpResponse(null, Response::HTTP_INTERNAL_ERROR);
        }
    }
    
    /**
     * Searches for movies by title.
     *
     * @return JsonResponse a JSON result.
     */
    public function moviesByTitle($title)
    {     
        // regular expression to match.
        $regex = sprintf('/.*%s.*/i', $title);
        // limit and skip to paginate.
        $limit  = $this->getLimit();
        $skip   = $this->getSkip();
        // sort and ordering of results.
        $orderby = $this->getOrderBy();
        $order   = $this->getOrder();
        
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Movie');
        $results = $queryBuilder->field('title')->equals(new \MongoRegex($regex))
                                ->limit($limit)
                                ->skip($skip)
                                ->sort($orderby, $order)
                                ->getQuery()
                                ->execute();
                   
        // 204 No Content: collection is empty
        if ($results->count() == 0) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        try {
            return new JsonResponse($this->createMovies($results));
        } catch (\InvalidArgumentException $e) {
            return new HttpResponse(null, Response::HTTP_INTERNAL_ERROR);
        }
    }
    
    
    /**
     * Searches for movies by genre.
     *
     * @return JsonResponse a JSON result.
     */
    public function moviesByGenre($genre)
    {     
        // regular expression to match.
        $regex = sprintf('/.*%s.*/i', $genre);
        // limit and skip to paginate.
        $limit  = $this->getLimit();
        $skip   = $this->getSkip();
        // sort and ordering of results.
        $orderby = $this->getOrderBy();
        $order   = $this->getOrder();
        
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Genre');
        $genre = $queryBuilder->field('genre')->equals(new \MongoRegex($regex))
                              ->getQuery()
                              ->getSingleResult();
                              
        // 204 No Content: no genre found
        if(!$genre) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Movie');
        $results = $queryBuilder->field('genres.id')->in(array($genre->getId()))
                                ->limit($limit)
                                ->skip($skip)
                                ->sort($orderby, $order)
                                ->getQuery()
                                ->execute();
                   
        // 204 No Content: collection is empty
        if ($results->count() == 0) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        try {
            return new JsonResponse($this->createMovies($results));
        } catch (\InvalidArgumentException $e) {
            return new HttpResponse(null, Response::HTTP_INTERNAL_ERROR);
        }
    }
    
    
    /**
     * Searches for movies by directory
     *
     * @return JsonResponse a JSON result.
     */
    public function moviesByDirector($director)
    {     
        // regular expression to match.
        $regex = sprintf('/.*%s.*/i', $director);
        // limit and skip to paginate.
        $limit  = $this->getLimit();
        $skip   = $this->getSkip();
        // sort and ordering of results.
        $orderby = $this->getOrderBy();
        $order   = $this->getOrder();
        
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Director');
        $director = $queryBuilder->field('name')->equals(new \MongoRegex($regex))
                              ->getQuery()
                              ->getSingleResult();
        
        // 204 No Content: no director found
        if(!$director) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Movie');
        $results = $queryBuilder->field('directors.id')->in(array($director->getId()))
                                ->limit($limit)
                                ->skip($skip)
                                ->sort($orderby, $order)
                                ->getQuery()
                                ->execute();
                   
        // 204 No Content: collection is empty
        if ($results->count() == 0) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        try {
            return new JsonResponse($this->createMovies($results));
        } catch (\InvalidArgumentException $e) {
            return new HttpResponse(null, Response::HTTP_INTERNAL_ERROR);
        }
    }
    
    
    /**
     * Searches for movies by actors.
     *
     * @return JsonResponse a JSON result.
     */
    public function moviesByActor($artist)
    {     
        // regular expression to match.
        $regex = sprintf('/.*%s.*/i', $artist);
        // limit and skip to paginate.
        $limit  = $this->getLimit();
        $skip   = $this->getSkip();
        // sort and ordering of results.
        $orderby = $this->getOrderBy();
        $order   = $this->getOrder();
        
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Artist');
        $artist = $queryBuilder->field('name')->equals(new \MongoRegex($regex))
                              ->getQuery()
                              ->getSingleResult();
        
        
        // 204 No Content: no artist found
        if(!$artist) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Movie');
        $results = $queryBuilder->field('genres.id')->in(array($artist->getId()))
                                ->limit($limit)
                                ->skip($skip)
                                ->sort($orderby, $order)
                                ->getQuery()
                                ->execute();
                   
        // 204 No Content: collection is empty
        if ($results->count() == 0) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        try {
            return new JsonResponse($this->createMovies($results));
        } catch (\InvalidArgumentException $e) {
            return new HttpResponse(null, Response::HTTP_INTERNAL_ERROR);
        }
    }
    
    /**
     * Searches for movies by year.
     *
     * @return JsonResponse a JSON result.
     */
    public function moviesByYear($year)
    {
        // 400 Bad Request: invalid argument provided.
        if (!is_numeric($year)) {
            return new HttpResponse(null, Response::HTTP_BAD_REQUEST);
        }
        
        // limit and skip to paginate.
        $limit  = $this->getLimit();
        $skip   = $this->getSkip();
        // sort and ordering of results.
        $orderby = $this->getOrderBy();
        $order   = $this->getOrder();
        
        $queryBuilder = $this->documentManager->createQueryBuilder('\Movies\Models\Mongodb\Movie');
        $results = $queryBuilder->field('year')->equals((int) $year)
                                ->limit($limit)
                                ->skip($skip)
                                ->sort($orderby, $order)
                                ->getQuery()
                                ->execute();

        // 204 No Content: zero results found
        if ($results->count() == 0) {
            return new HttpResponse(null, Response::HTTP_NO_CONTENT);
        }
        
        try {
            return new JsonResponse($this->createMovies($results));
        } catch (\InvalidArgumentException $e) {
            return new HttpResponse(null, Response::HTTP_INTERNAL_ERROR);
        }
    }
    
    /**
     * Returns the limit from the $_GET superglobal.
     *
     * @return int the limit, or maximum number of movies to display if no limit is given.
     */
    private function getLimit()
    {    
        $limit = $this->getRequest()->get('limit', self::MAX_RESULTS);
        if (is_numeric($limit) && $limit > 0 && $limit <= self::MAX_RESULTS) {
            return (int) $limit;
        }
        
        return self::MAX_RESULTS;
    }
    
    /**
     * Returns the offset from the $_GET superglobal.
     *
     * @return int the offset, or 0 if no offset is given.
     */
    private function getOffset()
    {        
        $offset = $this->getRequest()->get('paged', 0);
        if (is_numeric($offset) && $offset >= 0) {
            return (int) $offset;
        }
        
        return 0;
    }
    
    /**
     * Returns the number of movies to skip.
     *
     * @return int the number of movies to skip
     * @see MovieController::getLimit()
     * @see MovieController::getOffset()
     */
    private function getSkip()
    {
        return ($this->getOffset() * $this->getLimit());
    }
    
    /**
     * Returns the order from the $_GET superglobal.
     *
     * @return string the order, if not provided 'asc' will be returned.
     * @link http://php.net/manual/en/mongocursor.sort.php MongoCursor::sort
     */
    private function getOrder()
    {
        $allowed = array(
            'asc'  =>  1,
            'desc' => -1,
        );
    
        $order = strtolower($this->getRequest()->get('order', 'asc'));
        if (in_array($order, array_keys($allowed))) {
            return $allowed[$order];
        }
        
        return reset($allowed);
    }
    
    /**
     * Returns the order by from the $_GET superglobal.
     *
     * @return string the order by, if not provided 'title' will be returned.
     */
    private function getOrderBy()
    {
        $orderby = strtolower($this->getRequest()->get('order_by', 'title'));
        if (in_array($orderby, array('title', 'year', 'duration', 'rated', 'id'))) {
            return $orderby;
        }
        
        return 'title';
    }
    
    /**
     * Returns an associative array containing the next and previous link.
     *
     * A possible return value for this method could be as following:
     *
     * array(
     *     'previous' => 'http://localhost/movies?paged=2',
     *     'next'     => 'http://localhost/movies?paged=3',
     * )
     *
     * @param int $resultsPerPage the number of results to display per page.
     * @param int $resultsCount (optional) the total number of results found.
     */
    private function createPaging($resultsCount = 0, $resultsPerPage = self::MAX_RESULTS)
    {
        $pager = new Pager($this->getRequest());
        $pager->setResultsCount($resultsCount)
              ->setResultsPerPage($resultsPerPage);
              
        $paging = array();
        if ($pager->hasPreviousPage()) {
            $paging['previous'] = $pager->getPreviousPage();
        }
        if ($pager->hasNextPage()) {
            $paging['next'] = $pager->getNextPage();
        }
        
        return $paging;
    }
    
    /**
     * Creates a collection of movies.
     *
     * Paging controls will be added if the number of results is larger than the number
     * of results that can be displayed on a single page.
     *
     * @param Cursor $results a collection of database results.
     * @return array a multidimensional array containing zero or more movies.
     * @throws InvalidArgumentException if the given argument is not an array or Traversable object.
     * @see MovieController::createMovie($result)
     */
    private function createMovies($results)
    {
        if (!is_array($results) && !($results instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects an array or Traversable object as argument; received "%d"',
                __METHOD__,
                (is_object($results) ? get_class($results) : gettype($results))
            ));
        }
    
        $response = array();
        foreach ($results as $result) {
            $response['movies'][] = $this->createMovie($result);
        }
        
        // add paging controls.
        $paging = $this->createPaging($results->count(), $this->getLimit());
        if (!empty($paging)) {
            $response['paging'] = $paging;
        }
        
        return $response;
    }
    
    /**
     * Creates an associative array containing the details of a movie.
     *
     * @param Movie $result a collection with database result.
     * @retun array an associative array containing movie details.
     * @throws InvalidArgumentException if the given argument is not a Movie object.
     */
    private function createMovie($result)
    {
        if (!($result instanceof Movie)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a Movies\Models\Mongodb\Movie object as argument; received "%d"',
                __METHOD__,
                (is_object($result) ? get_class($result) : gettype($result))
            ));
        }
    
        $movie = array();
        $movie['id']           = $result->getId();
        $movie['title']        = $result->getTitle();
        $movie['year']         = $result->getYear();
        $movie['rated']        = $result->getRating();
        //$movie['votes']      = $result->getVotes();
        $movie['release_date'] = $result->getReleaseDate();
        $movie['runtime']      = $result->getDuration();
        $movie['genres']       = $this->createGenres($result->getGenres());
        $movie['directors']    = $this->createDirectors($result->getDirectors());
        $movie['cast']         = $this->createCast($result->getCast());
        $movie['plot']         = $result->getDescription();
        $movie['language']     = $result->getLanguage();
        $movie['country']      = $result->getCountry();
        
        return $movie;
    }
    
    /**
     * Creates an associative array with genres.
     *
     * @param PersistentCollection $result a collection with database result.
     * @retun array an associative array containing genres.
     */
    private function createGenres($result)
    {    
        $genres = array();
        foreach ($result->getIterator() as $genre) {
            $genres[] = $genre->getGenre();
        }
        
        return $genres;
    }
    
    /**
     * Creates an associative array with the names of directors.
     *
     * @param PersistentCollection $result a collection with database result.
     * @retun array an associative array containing directors.
     */
    private function createDirectors($result)
    {
    
        $directors = array();
        foreach ($result->getIterator() as $director) {
            $directors[] = $director->getName();
        }
        
        return $directors;
    }
    
    /**
     * Creates an associative array with the names of cast memebers.
     *
     * @param PersistentCollection $result a collection with database result.
     * @retun array an associative array containing cast members.
     */
    private function createCast($result)
    {
        $cast = array();
        foreach ($result->getIterator() as $artist) {
            $cast[] = $artist->getName();
        }
        
        return $cast;
    }
}
