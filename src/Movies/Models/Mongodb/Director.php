<?php

namespace Movies\Models\Mongodb;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
/**
 * Description of Director
 *
 * @author kevinpostma
 * @ODM\Document
 * @ODM\UniqueIndex(keys={"name"="asc"})
 */
class Director {
    
    /** @ODM\Id */
    private $id;

    /** @ODM\String */
    private $name;
    
    public function getId() {
	return $this->id;
    }

    public function getName() {
	return $this->name;
    }

    public function setId($id) {
	$this->id = $id;
    }

    public function setName($name) {
	$this->name = $name;
    }

}
