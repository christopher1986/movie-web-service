<?php

namespace Movies\Models\Mongodb;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
/**
 * Description of Genre
 *
 * @author kevinpostma
 * @ODM\Document
 * @ODM\UniqueIndex(keys={"genre"="asc"})
 */
class Genre {
    
    /** @ODM\Id */
    private $id;

    /** @ODM\String */
    private $genre;
    
    public function getId() {
	return $this->id;
    }

    public function getGenre() {
	return $this->genre;
    }

    public function setId($id) {
	$this->id = $id;
    }

    public function setGenre($genre) {
	$this->genre = $genre;
    }

}
