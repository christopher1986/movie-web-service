<?php

namespace Movies\Models\Mongodb;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Description of Movie
 *
 * @author kevinpostma
 * @ODM\Document
 * @ODM\UniqueIndex(keys={"title"="asc", "year"="asc"})
 */
class Movie {
    
    /** @ODM\Id */
    private $id;
    
    /** @ODM\String */
    private $title;
    
    /** @ODM\String */
    private $description;
    
    /** @ODM\ReferenceMany(targetDocument="Movies\Models\Mongodb\Genre", cascade="all") */
    private $genres = array();
    
    /** @ODM\Date */
    private $releaseDate;
    
    /** @ODM\Int */
    private $year;
    
    /** @ODM\Int */
    private $duration;
    
    /** @ODM\Int */
    private $rating;
    
    /** @ODM\ReferenceMany(targetDocument="Movies\Models\Mongodb\Director", cascade="all") */
    private $directors = array();
    
    /** @ODM\ReferenceMany(targetDocument="Movies\Models\Mongodb\Director", cascade="all") */
    private $writers = array();
    
    /** @ODM\ReferenceMany(targetDocument="Movies\Models\Mongodb\Artist", cascade="all") */
    private $cast = array();
    
    /** @ODM\String */
    private $country;
    
    /** @ODM\String */
    private $language;
    
    public function __construct() {
	
    }
    
    public function isValid() {
	if(strlen($this->getTitle()) > 0 && $this->getYear() > 0) {
	    return true;
	}
	return false;
    }
    
    function getId() {
	return $this->id;
    }

    function getTitle() {
	return $this->title;
    }

    function getDescription() {
	return $this->description;
    }

    function getGenres() {
	return $this->genres;
    }

    function getReleaseDate() {
	return $this->releaseDate;
    }

    function getYear() {
	return $this->year;
    }

    function getDuration() {
	return $this->duration;
    }

    function getRating() {
	return $this->rating;
    }

    function getDirectors() {
	return $this->directors;
    }

    function getWriters() {
	return $this->writers;
    }

    function getCast() {
	return $this->cast;
    }

    function getCountry() {
	return $this->country;
    }

    function getLanguage() {
	return $this->language;
    }

    function setId($id) {
	$this->id = $id;
    }

    function setTitle($title) {
	$this->title = $title;
    }

    function setDescription($description) {
	$this->description = $description;
    }

    function setGenres($genres) {
	$this->genres = $genres;
    }

    function setReleaseDate($releaseDate) {
	$this->releaseDate = $releaseDate;
    }

    function setYear($year) {
	$this->year = $year;
    }

    function setDuration($duration) {
	$this->duration = $duration;
    }

    function setRating($rating) {
	$this->rating = $rating;
    }

    function setDirectors($directors) {
	$this->directors = $directors;
    }

    function setWriters($writers) {
	$this->writers = $writers;
    }

    function setCast($cast) {
	$this->cast = $cast;
    }

    function setCountry($country) {
	$this->country = $country;
    }

    function setLanguage($language) {
	$this->language = $language;
    }

    
//    public function toArray() {
//        if($this->)
//    }
}
