<?php

namespace Movies\Helper;

use Framework\Util\Strings;
use Slim\Http\Request;

/**
 * The Pager helper determines whether a next and previous link should be displayed
 * based on the total number of results found and the number of results to display
 * per page.
 *
 * @autor Chris Harris
 * @version 0.0.1
 */
class Pager
{
    /**
     * Slim HTTP Request.
     *
     * @var Request;
     */
    private $request;
    
    /**
     * The number of results per page.
     *
     * @var int
     */
    private $resultsPerPage = 25;
    
    /**
     * The number of results found.
     *
     * @var int
     */
    private $resultsCount = 0;
    
    /**
     * Construct the Pager.
     *
     * @param Request a Slim HTTP Request.
     */
    public function __construct(Request $request)
    {
        $this->setRequest($request);
    }
    
    /**
     * Set the Request object.
     *
     * @param Request $request a Slim HTTP Request.
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * Returns the Request object.
     *
     * @return Request a Slim HTTP Request.
     * @see Slim\Slim::request()
     */
    public function getRequest()
    {
        return $this->request;
    }
    
    /**
     * Set the number of results to display per page.
     *
     * @param int $resultsPerPage the number of results to display per page.
     * @return Pagination 
     * @throws InvalidArgumentException if the given argument is smaller than 1.
     */
    public function setResultsPerPage($resultsPerPage = 25)
    {
        if (!is_numeric($resultsPerPage) || $resultsPerPage < 1) {
            throw new \InvalidArgumentException(sprintf(
                '%s: result per page must be 1 or greater; received "%s"',
                __METHOD__,
                (is_object($resultsPerPage) ? get_class($resultsPerPage) : gettype($resultsPerPage))
            ));
        }
        
        $this->resultsPerPage = (int) $resultsPerPage;
        
        return $this;
    }
    
    /**
     * Returns the number of results to display per page.
     *
     * @return int the number of results to display per page. 
     */
    public function getResultsPerPage()
    {
        return $this->resultsPerPage;
    }
    
    /**
     * Set the total number of results found.
     *
     * @param int $resultsCount the number of results found.
     * @return Pagination
     * @throws InvalidArgumentException if the given argument is a negative number.
     */
    public function setResultsCount($resultsCount = 0)
    {
        if (!is_numeric($resultsCount) || $resultsCount < 0) {
            throw new \InvalidArgumentException(sprntf(
                '%s: total results must be a positive number; received "%s"',
                __METHOD__,
                (is_object($totalResults) ? get_class($totalResults) : gettype($totalResults))
            ));
        }        
        
        $this->resultsCount = (int) $resultsCount;
        
        return $this;
    }
    
    /**
     * Returns the total number of results found.
     *
     * @return int the number of results found.
     */
    public function getResultsCount()
    {
        return $this->resultsCount;
    }
    
    /**
     * Returns the current page number.
     *
     * @param int $default the default page to return if the current page is not set.
     * @return int the current page.
     * @throws InvalidArgumentException if the given argument is smaller than 1.
     * @see Request::get($key, $default)
     */
    public function getCurrentPage($default = 0)
    {
        if (!is_numeric($default) || $default < 0) {
            throw new \InvalidArgumentException(sprntf(
                '%s: the current page must be 1 or greater; received "%s"',
                __METHOD__,
                (is_object($default) ? get_class($default) : gettype($default))
            ));
        }
    
        return (int) $this->getRequest()->get('paged', $default);
    }
    
    /**
     * Returns the total number of pages available.
     *
     * The total number of pages is calculated using the results count and the
     * maximum number of results to display on a single page. So for example
     * if the results count is 75 and the maximum number of results to display
     * on a page is 25 the number page count would 3 (75 % 25 = 3)
     *
     * @return int the number of pages available.
     * @see Pager::getResultsCount()
     * @see Pager::getResultsPerPage()
     */
    public function getPageCount()
    {
        $resultsCount   = $this->getResultsCount();
        $resultsPerPage = $this->getResultsPerPage();
        
        $pageCount = 1;
        if ($resultsCount > 0 && ($resultsCount > $resultsPerPage)) {
            $pageCount = ($resultsCount / $resultsPerPage);
        }
        return ceil($pageCount);
    }
    
    /**
     * Returns true if a link to a previous or next page is available.
     *
     * @return bool true if a previous or next page is vailable.
     */
    public function hasPages()
    {
        return ($this->hasPreviousPage() || $this->hasNextPage());
    }
    
    /**
     * Returns true if a link to a previous page is available.
     *
     * @return bool true if a previous page is available, false otherwise.
     */
    public function hasPreviousPage()
    {
        return ($this->getCurrentPage() > 0);
    }
    
    /**
     * Returns true if a link to a previous page is available.
     *
     * @return bool true if a previous page is available, false otherwise.
     */
    public function hasNextPage()
    {
        return ($this->getCurrentPage() < $this->getPageCount());
    }
    
    /**
     * Returns if available the next page.
     *
     * @return string the link to the next page, of empty string on failure.
     */
    public function getNextPage()
    {
        $nextPage  = ($this->getCurrentPage() + 1);
        $pageCount = $this->getPageCount();
    
        $link = '';
        if ($this->hasNextPage()) {
            if ($nextPage < $pageCount) {
                $link = $this->getPaginationUrl($nextPage);
            } else {
                $link = $this->getPaginationUrl($pageCount);
            }
        }
        
        return $link;
    }
    
    /**
     * Returns if available the previous page.
     *
     * @return string the link to the previous page, of empty string on failure.
     */
    public function getPreviousPage()
    {
        $previousPage = ($this->getCurrentPage() - 1);
        $pageCount    = $this->getPageCount();
    
        $link = '';
        if ($this->hasPreviousPage()) {
            if ($previousPage >= 0 && ($previousPage < $pageCount)) {
                $link = $this->getPaginationUrl($previousPage);
            } else {
                $link = $this->getPaginationUrl($pageCount - 1);
            }
        }
        
        return $link;
    }
    
    /**
     * Returns the paging url which contains the given page number. 
     *
     * A possible pagination url may look as following:
     * 
     * http://localhost/movies?paged=2
     *
     * @param int $page the page number.
     * @return string the pagination url.
     */
    private function getPaginationUrl($page)
    {
        $params = $_GET;
        $params['paged'] = $page;
                
        return $this->getRequestUrl() . '?' . http_build_query($params, '', '&');        
    }
    
    /**
     * Returns the URL the client used to make the request.
     *
     * @return string the request url made by the client.
     */
    private function getRequestUrl()
    {
        $request = $this->getRequest();
        return $request->getScheme() . '://' . $request->getHost() . $request->getRootUri() . $request->getResourceUri();   
    }
}
