<?php

namespace Movies;

use Framework\Config\Configurable;
use Framework\Loader\AutoloaderAwareInterface;
use Framework\ServiceLocator\ServiceAwareInterface;
use Framework\ServiceLocator\ServiceLocatorInterface;

/**
 * Called when the module is loaded for the  first ime.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class Module implements AutoloaderAwareInterface, ServiceAwareInterface, Configurable
{
    /**
     * A locator that contains and creates services.
     *
     * @var ServiceLocatorInterface
     */
    private $locator;

    /**
     * {@inheritDoc}
     */
    public function setServiceLocator(ServiceLocatorInterface $locator)
    {
        $this->locator = $locator;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getServiceLocator()
    {
        return $this->locator;
    }

    /**
     * {@inheritDoc}
     */
    public function registerAutoloader()
    {
        return array(
            'Framework\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    /**
     * {@inheritDoc}
     */
    public function registerConfiguration()
    {
        return sprintf('%s/config/routes.php', __DIR__);
    }
}
