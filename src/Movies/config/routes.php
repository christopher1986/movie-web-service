<?php

return array(
    'routes' => array(
        array(
            'route' => array(
                'pattern'  => '/movies',
                'callable' => 'Movies\Controller\MovieController:getAll',
                'method'   => 'get',
            ),
        ),
        array(
            'route' => array(
                'pattern'  => '/movies/:id',
                'callable' => 'Movies\Controller\MovieController:get',
                'method'   => 'get',
            ),
        ),
        array(
            'route' => array(
                'pattern'  => '/movies/year/:year',
                'callable' => 'Movies\Controller\MovieController:moviesByYear',
                'method'   => 'get',
            ),
        ),
        array(
            'route' => array(
                'pattern' => '/movies/title/:title',
                'callable' => 'Movies\Controller\MovieController:moviesByTitle',
                'method'  => 'get',
            ),
        ),
        array(
            'route' => array(
                'pattern'  => '/movies/genre/:genre',
                'callable' => 'Movies\Controller\MovieController:moviesByGenre',
                'method'   => 'get',
            ),
        ),
        array(
            'route' => array(
                'pattern'  => '/movies/director/:director',
                'callable' => 'Movies\Controller\MovieController:moviesByDirector',
                'method'   => 'get',
            ),
        ),
        array(
            'route' => array(
                'pattern'  => '/movies/actor/:actor',
                'callable' => 'Movies\Controller\MovieController:moviesByActor',
                'method'   => 'get',
            ),
        ),
    ),
);
