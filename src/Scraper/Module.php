<?php

namespace Scraper;

use Framework\Config\ConfigInterface;
use Framework\EventDispatcher\EventDispatcherInterface;
use Framework\Loader\AutoloaderAwareInterface;
use Framework\ServiceLocator\ServiceAwareInterface;
use Framework\ServiceLocator\ServiceLocatorInterface;

/**
 * Called when the module is loaded for the  first ime.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class Module implements AutoloaderAwareInterface, ServiceAwareInterface
{
    /**
     * A locator that contains and creates services.
     *
     * @var ServiceLocatorInterface
     */
    private $locator;

    /**
     * {@inheritDoc}
     */
    public function setServiceLocator(ServiceLocatorInterface $locator)
    {
        $this->locator = $locator;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getServiceLocator()
    {
        return $this->locator;
    }

    /**
     * {@inheritDoc}
     */
    public function registerAutoloader()
    {
        return array(
            'Framework\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
