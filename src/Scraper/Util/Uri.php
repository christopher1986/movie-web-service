<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Scraper\Util;

/**
 * Description of newPHPClass
 *
 * @author Kevin
 */
class Uri
{
	
	public function endsWith($haystack, $needle) {

		//Get the length of the end string
		$strLen = strlen($needle);

		//Look at the end of FullStr for the substring the size of EndStr
		$haystackEnd = substr($haystack, strlen($haystack) - $strLen);

		//If it matches, it does end with EndStr
		return $haystackEnd == $needle;
	}

	public static function getDomain($url) {

		$url = strtolower($url);

		//Remove querie (domain?querie)
		if (strpos($url, "?") > 0) {
			$url = substr($url, 0, strpos($url, "?"));
		}

		//Remove the http://
		$url = str_replace("http://", "", $url);

		//Remove the https://
		$url = str_replace("https://", "", $url);

		//Get everything before the /
		if (strpos($url, "/") !== False) {
			$url = substr($url, 0, strpos($url, "/"));
		}

		$parts = explode(".", $url);
		$index = 0;

		//Determin how many points the url has
		if (count($parts) == 2) {
			$index = 0;
		} elseif (count($parts) == 3) {
			if (strlen($parts[1]) > 3) {
				$index = 1;
			} else {
				$index = 0;
			}
		} elseif (count($parts) > 3) {
			$last = count($parts) - 1;
			$both = strlen($parts[$last]) + strlen($parts[$last - 1]);
			if (strlen($parts[$last - 1]) > 3) {
				$index = $last - 1;
			} elseif ($both > 5) {
				$index = $last;
			} else {
				$index = $last - 2;
			}
		}
		$parts = array_slice($parts, $index);

		//Return the domain name
		return implode(".", $parts);
	}

	public static function getHostName($url) {

		$url = strtolower($url);

		preg_match('@^(?:http://|https://)?([^/]+)@i', $url, $matches);
		$host = $matches[1];

		return $host;
	}

	public function explodeURL($url) {

		//First define what TLD;s we support (Europe + Internaional)
		$tlds = array(
			//Europe
			".ad",
			".ac",
			".al",
			".gv.at", ".ac.at", ".co.at", ".or.at", ".at",
			".be",
			".bg",
			".ch",
			".ac.cy", ".net.cy", ".gov.cy", ".org.cy", ".pro.cy", ".name.cy", ".ekloges.cy", ".tm.cy", ".ltd.cy", ".biz.cy", ".press.cy", ".parliament.cy", ".com.cy", ".cy",
			".cz",
			".de",
			".dk",
			".pri.ee", ".org.ee", ".med.ee", ".fie.ee", ".com.ee", ".ee",
			".com.es", ".nom.es", ".org.es", ".gob.es", ".edu.es", ".es",
			".fi",
			".fo",
			".tm.fr", ".presse.fr", ".prd.fr", ".nom.fr", ".gouv.fr", ".com.fr", ".asso.fr", ".fr",
			".gb",
			".com.gi", ".ltd.gi", ".gov.gi", ".mod.gi", ".edu.gi", ".org.gi", ".gi",
			".co.gg", ".net.gg", ".org.gg", ".gg",
			".org.gr", ".net.gr", ".gov.gr", ".edu.gr", ".com.gr", ".gr",
			".name.hr", ".iz.hr", ".from.hr", ".hr",
			".2000.hu", ".agrar.hu", ".bolt.hu", ".casino.hu", ".city.hu", ".co.hu", ".erotica.hu", ".erotika.hu", ".film.hu", ".forum.hu", ".games.hu", ".hotel.hu", ".info.hu", ".ingatlan.hu", ".jogasz.hu", ".konyvelo.hu", ".lakas.hu", ".media.hu", ".news.hu", ".org.hu", ".priv.hu", ".reklam.hu", ".sex.hu", ".shop.hu", ".sport.hu", ".suli.hu", ".szex.hu", ".tm.hu", ".tozsde.hu", ".utazas.hu", ".video.hu", ".hu",
			".gov.ie", ".ie",
			".plc.im", ".org.im", ".net.im", ".ltd.im", ".gov.im", ".com.im", ".co.im", ".ac.im", ".im",
			".is",
			".it",
			".li",
			".lt",
			".lu",
			".org.lv", ".net.lv", ".mil.lv", ".id.lv", ".gov.lv", ".edu.lv", ".conf.lv", ".com.lv", ".asn.lv", ".lv",
			".mc",
			".md",
			".me",
			".gov.mt", ".edu.mt", ".net.mt", ".org.mt", ".com.mt", ".mt",
			".com.mk", ".org.mk", ".net.mk", ".edu.mk", ".gov.mk", ".inf.mk", ".name.mk", ".mk",
			".nl",
			".priv.no", ".no",
			".com.pl", ".org.pl", ".biz.pl", ".info.pl", ".edu.pl", ".pl",
			".publ.pt", ".org.pt", ".nome.pt", ".net.pt", ".int.pt", ".gov.pt", ".edu.pt", ".com.pt", ".pt",
			".www.ro", ".tm.ro", ".store.ro", ".rec.ro", ".org.ro", ".nt.ro", ".nom.ro", ".info.ro", ".firm.ro", ".com.ro", ".arts.ro", ".ro",
			".pp.ru", ".org.ru", ".net.ru", ".com.ru", ".ru",
			".ab.se", ".ac.se", ".bd.se", ".c.se", ".d.se", ".e.se", ".f.se", ".g.se", ".h.se", ".i.se", ".k.se", ".m.se", ".mil.se", ".n.se", ".o.se", ".org.se", ".parti.se", ".pp.se", ".press.se", ".s.se", ".t.se", ".tm.se", ".u.se", ".w.se", ".x.se", ".y.se", ".se",
			".si",
			".sk",
			".sm",
			".co.rs", ".org.rs", ".edu.rs", ".ac.rs", ".gov.rs", ".in.rs", ".rs",
			".com.ba", ".co.ba", ".rs.ba", ".ba",
			".com.tr", ".gen.tr", ".org.tr", ".biz.tr", ".info.tr", ".av.tr", ".dr.tr", ".pol.tr", ".bel.tr", ".tsk.tr", ".bbs.tr", ".k12.tr", ".edu.tr", ".name.tr", ".net.tr", ".gov.tr", ".web.tr", ".tel.tr", ".tv.tr", ".nc.tr", ".tr",
			".tc",
			".cherkassy.ua", ".chernigov.ua", ".chernovtsy.ua", ".ck.ua", ".cn.ua", ".com.ua", ".crimea.ua", ".cv.ua", ".dn.ua", ".dnepropetrovsk.ua", ".donetsk.ua", ".dp.ua", ".edu.ua", ".gov.ua", ".if.ua", ".in.ua", ".ivano-frankivsk.ua", ".kh.ua", ".kharkov.ua", ".kherson.ua", ".khmelnitskiy.ua", ".kiev.ua", ".kirovograd.ua", ".km.ua", ".kr.ua", ".ks.ua", ".kv.ua", ".lg.ua", ".lugansk.ua", ".lutsk.ua", ".lviv.ua", ".mk.ua", ".net.ua", ".nikolaev.ua", ".od.ua", ".odessa.ua", ".org.ua", ".pl.ua", ".poltava.ua", ".rovno.ua", ".rv.ua", ".sebastopol.ua", ".sumy.ua", ".te.ua", ".ternopil.ua", ".uzhgorod.ua", ".vinnica.ua", ".vn.ua", ".zaporizhzhe.ua", ".zhitomir.ua", ".zp.ua", ".zt.ua", ".ua",
			".co.uk", ".ltd.uk", ".me.uk", ".net.uk", ".nic.uk", ".org.uk", ".plc.uk", ".sch.uk", ".ac.uk", ".uk",
			".va",
			//North America
			".ag",
			".ai",
			".com.bs", ".net.bs", ".org.bs", ".edu.bs", ".gov.bs", ".we.bs", ".bs",
			".co.bb", ".com.bb", ".net.bb", ".org.bb", ".gov.bb", ".edu.bb", ".info.bb", ".store.bb", ".tv.bb", ".biz.bb", ".bb",
			".com.bz", ".edu.bz", ".gov.bz", ".net.bz", ".org.bz", ".bz",
			".ab.ca", ".bc.ca", ".mb.ca", ".nb.ca", ".nf.ca", ".nl.ca", ".ns.ca", ".nt.ca", ".nu.ca", ".on.ca", ".pe.ca", ".qc.ca", ".sk.ca", ".yk.ca", ".ca",
			".ac.cr", ".co.cr", ".ed.cr", ".fi.cr", ".go.cr", ".or.cr", ".sa.cr", ".cr",
			".com.cu", ".edu.cu", ".org.cu", ".net.cu", ".gov.cu", ".inf.cu", ".cu",
			".com.dm", ".net.dm", ".org.dm", ".dm",
			".art.do", ".com.do", ".edu.do", ".gob.do", ".mil.do", ".net.do", ".org.do", ".sld.do", ".web.do", ".do",
			".edu.sv", ".gob.sv", ".com.sv", ".org.sv", ".red.sv", ".sv",
			".gd",
			".gl",
			".com.gt", ".edu.gt", ".net.gt", ".gob.gt", ".org.gt", ".mil.gt", ".ind.gt", ".gt",
			".ht",
			".net.hn", ".org.hn", ".edu.hn", ".gob.hn", ".com.hn", ".hn",
			".com.jm", ".net.jm", ".org.jm", ".edu.jm", ".gov.jm", ".mil.jm",
			".com.mx", ".net.mx", ".org.mx", ".edu.mx", ".gob.mx", ".mx",
			".com.ms", ".edu.ms", ".gov.ms", ".net.ms", ".org.ms", ".ms",
			".gob.ni", ".co.ni", ".ac.ni", ".org.ni", ".nom.ni", ".net.ni", ".mil.ni", ".ni",
			".net.pa", ".com.pa", ".ac.pa", ".sld.pa", ".gob.pa", ".edu.pa", ".org.pa", ".abo.pa", ".ing.pa", ".med.pa", ".nom.pa", ".pa",
			".net.kn", ".org.kn", ".edu.kn", ".gov.kn", ".kn",
			".lc",
			".vc",
			".vg",
			".gp",
			"co.tt", "com.tt", "org.tt", "net.tt", "biz.tt", "info.tt", "pro.tt", "int.tt", "coop.tt", "jobs.tt", "mobi.tt", "travel.tt", "museum.tt", "aero.tt", "cat.tt", "tel.tt", "name.tt",
			".dni.us", ".fed.us", ".isa.us", ".kids.us", ".nsn.us", ".us",
			//South Amerika
			".adm.br", ".adv.br", ".agr.brv", ".am.br", ".arq.br", ".art.br", ".ato.br", ".b.br", ".bio.br", ".blog.br", ".bmd.br", ".cim.br", ".cng.br", ".cnt.br", ".com.br", ".coop.br", ".ecn.br", ".edu.br", ".eng.br", ".esp.br", ".etc.br", ".eti.br", ".far.br", ".flog.br", ".fm.br", ".fnd.br", ".fot.br", ".fst.br", ".g12.br", ".ggf.br", ".gov.br", ".imb.br", ".ind.br", ".inf.br", ".jor.br", ".jus.br", ".lel.br", ".mat.br", ".med.br", ".mil.br", ".mus.br", ".net.br", ".nom.br", ".not.br", ".ntr.br", ".odo.br", ".org.br", ".ppg.br", ".pro.br", ".psc.br", ".psi.br", ".qsl.br", ".radio.br", ".rec.br", ".slg.br", ".srv.br", ".taxi.br", ".teo.br", ".tmp.br", ".trd.br", ".tur.br", ".tv.br", ".vet.br", ".vlog.br", ".wiki.br", ".zlg.br", ".br",
			".com.ar", ".edu.ar", ".gob.ar", ".gov.ar", ".int.ar", ".mil.ar", ".net.ar", ".org.ar", ".tur.ar", ".ar",
			".com.bo", ".net.bo", ".org.bo", ".tv.bo", ".mil.bo", ".int.bo", ".gob.bo", ".edu.bo", ".bo",
			".cl",
			".com.co", ".org.co", ".edu.co", ".gov.co", ".net.co", ".mil.co", ".nom.co", ".co",
			".com.ec", ".info.ec", ".net.ec", ".fin.ec", ".med.ec", ".pro.ec", ".org.ec", ".edu.ec", ".gob.ec", ".gov.ec", ".mil.ec", ".ec",
			".co.gy", ".com.gy", ".org.gy", ".net.gy", ".edu.gy", ".gov.gy", ".gy",
			".org.py", ".edu.py", ".mil.py", ".gov.py", ".vnet.py", ".com.py", ".coop.py", ".py",
			".edu.pe", ".gob.pe", ".nom.pe", ".mil.pe", ".sld.pe", ".org.pe", ".com.pe", ".net.pe", ".pe",
			".sr",
			".com.uy", ".edu.uy", ".gub.uy", ".net.uy", ".mil.uy", ".org.uy", ".uy",
			".com.ve", ".net.ve", ".org.ve", ".info.ve", ".co.ve", ".web.ve", ".gob.ve", ".edu.ve", ".mil.ve", ".tec.ve", ".ve",
			".co.fk", ".org.fk", ".gov.fk", ".ac.fk", ".nom.fk", ".net.fk", ".fk",
			".gs",
			//Oceanië
			".school.nz", ".org.nz", ".net.nz", ".maori.nz", ".gen.nz", ".geek.nz", ".co.nz", ".ac.nz", ".nz",
			".id.au", ".asn.au", ".csiro.au", ".gov.au", ".edu.au", ".org.au", ".net.au", ".com.au", ".au",
			".cx",
			".ac.fj", ".biz.fj", ".com.fj", ".info.fj", ".mil.fj", ".name.fj", ".net.fj", ".org.fj", ".pro.fj", ".fj",
			".ki",
			".mh",
			".fm",
			".edu.nr", ".gov.nr", ".biz.nr", ".info.nr", ".net.nr", ".org.nr", ".com.nr", ".nr",
			".pw",
			".com.pg", ".net.pg", ".ac.pg", ".gov.pg", ".mil.pg", ".org.pg",
			".in.pn", ".co.pn", ".eu.pn", ".org.pn", ".net.pn", ".me.pn", ".pn",
			".biz.pr", ".com.pr", ".edu.pr", ".gov.pr", ".info.pr", ".isla.pr", ".name.pr", ".net.pr", ".org.pr", ".pro.pr", ".est.pr", ".prof.pr", ".ac.pr", ".pr",
			".org.ws", ".gov.ws", ".edu.ws", ".ws",
			".com.sb", ".net.sb", ".edu.sb", ".org.sb", ".gov.sb", ".sb",
			".to",
			".tv",
			".sx",
			".tf",
			".co.sh", ".com.sh", ".org.sh", ".gov.sh", ".edu.sh", ".net.sh", ".nom.sh", ".sh",
			".vu",
			//Asia
			".com.af", ".edu.af", ".gov.af", ".net.af", ".org.af", ".af",
			".am",
			".com.az", ".net.az", ".int.az", ".gov.az", ".org.az", ".edu.az", ".info.az", ".pp.az", ".mil.az", ".name.az", ".pro.az", ".biz.az", ".az",
			".com.bh", ".info.bh", ".cc.bh", ".edu.bh", ".biz.bh", ".net.bh", ".org.bh", ".gov.bh", ".bh",
			".com.bd", ".edu.bd", ".ac.bd", ".net.bd", ".gov.bd", ".org.bd", ".mil.bd", ".bd",
			".bt",
			".com.bn", ".net.bn", ".org.bn", ".edu.bn", ".gov.bn", ".bn",
			".per.kh", ".com.kh", ".edu.kh", ".gov.kh", ".mil.kh", ".net.kh", ".org.kh", ".kh",
			".ac.cn", ".com.cn", ".edu.cn", ".gov.cn", ".mil.cn", ".net.cn", ".org.cn", ".cn",
			".com.hk", ".org.hk", ".net.hk", ".edu.hk", ".gov.hk", ".idv.hk", ".hk",
			".com.eg", ".edu.eg", ".eun.eg", ".gov.eg", ".info.eg", ".mil.eg", ".name.eg", ".net.eg", ".org.eg", ".sci.eg", ".tv.eg", ".eg",
			".com.ph", ".net.ph", ".org.ph", ".i.ph", ".gov.ph", ".mil.ph", ".edu.ph", ".ph",
			".ge",
			".nic.in", ".co.in", ".firm.in", ".net.in", ".org.in", ".gen.in", ".ind.in", ".ac.in", ".edu.in", ".res.in", ".ernet.in", ".gov.in", ".mil.in", ".in",
			".ac.id", ".co.id", ".net.id", ".or.id", ".web.id", ".sch.id", ".mil.id", ".go.id", ".id",
			".gov.iq", ".edu.iq", ".com.iq", ".mil.iq", ".org.iq", ".net.iq", ".iq",
			".ac.ir", ".co.ir", ".gov.ir", ".id.ir", ".net.ir", ".org.ir", ".sch.ir", ".ir",
			".ac.il", ".co.il", ".org.il", ".net.il", ".k12.il", ".gov.il", ".muni.il", ".idf.il", ".il",
			".ac.jp", ".ad.jp", ".co.jp", ".ed.jp", ".go.jp", ".gr.jp", ".lg.jp", ".ne.jp", ".or.jp", ".jp",
			".com.ye", ".co.ye", ".ltd.ye", ".me.ye", ".net.ye", ".org.ye", ".plc.ye", ".gov.ye", ".ye",
			".com.jo", ".net.jo", ".gov.jo", ".edu.jo", ".org.jo", ".mil.jo", ".name.jo", ".edu.jo", ".jo",
			".com.kz", ".edu.kz", ".gov.kz", ".mil.kz", ".net.kz", ".org.kz", ".kz",
			".org.kg", ".net.kg", ".com.kg", ".edu.kg", ".gov.kg", ".mil.kg", ".kg",
			".ac.kr", ".busan.kr", ".chungbuk.kr", ".chungnam.kr", ".co.kr", ".daegu.kr", ".daejeon.kr", ".es.kr", ".gangwon.kr", ".go.kr", ".gwangju.kr", ".gyeongbuk.kr", ".gyeonggi.kr", ".gyeongnam.kr", ".hs.kr", ".incheon.kr", ".jeju.kr", ".jeonbuk.kr", ".jeonnam.kr", ".kg.kr", ".mil.kr", ".ms.kr", ".ne.kr", ".or.kr", ".pe.kr", ".re.kr", ".sc.kr", ".seoul.kr", ".ulsan.kr", ".kr",
			".net.kw", ".com.kw", ".edu.kw", ".gov.kw", ".org.kw", ".kw",
			".la",
			".com.lb", ".edu.lb", ".gov.lb", ".net.lb", ".org.lb", ".lb",
			".aero.mv", ".biz.mv", ".com.mv", ".coop.mv", ".edu.mv", ".gov.mv", ".info.mv", ".int.mv", ".mil.mv", ".museum.mv", ".name.mv", ".net.mv", ".org.mv", ".pro.mv", ".mv",
			".com.my", ".net.my", ".org.my", ".gov.my", ".edu.my", ".mil.my", ".name.my", ".my",
			".gov.mn", ".edu.mn", ".org.mn", ".mn",
			".mm",
			".com.np", ".org.np", ".edu.np", ".net.np", ".gov.np", ".mil.np", ".np",
			".kp",
			".uz",
			".com.om", ".edu.om", ".gov.om", ".net.om", ".org.om", ".med.om", ".co.om", ".om",
			".tl",
			".tp",
			".com.pk", ".net.pk", ".edu.pk", ".org.pk", ".fam.pk", ".biz.pk", ".web.pk", ".gov.pk", ".gob.pk", ".gok.pk", ".gon.pk", ".gop.pk", ".gos.pk", ".gog.pk", ".pk",
			".com.ps", ".net.ps", ".org.ps", ".edu.ps", ".gov.ps", ".plo.ps", ".sec.ps", ".ps",
			".qa",
			".ac.ru", ".com.ru", ".edu.ru", ".gov.ru", ".int.ru", ".mil.ru", ".net.ru", ".org.ru", ".pp.ru",
			".by",
			".su",
			".com.sa", ".edu.sa", ".sch.sa", ".med.sa", ".gov.sa", ".net.sa", ".org.sa", ".pub.sa", ".sa",
			".com.sg", ".net.sg", ".org.sg", ".gov.sg", ".edu.sg", ".per.sg", ".idn.sg", ".sg",
			".gov.lk", ".sch.lk", ".net.lk", ".int.lk", ".com.lk", ".org.lk", ".edu.lk", ".ngo.lk", ".soc.lk", ".web.lk", ".ltd.lk", ".assn.lk", ".grp.lk", ".hotel.lk", ".lk",
			".sy",
			".ac.tj", ".aero.tj", ".biz.tj", ".co.tj", ".com.tj", ".coop.tj", ".dyn.tj", ".edu.tj", ".go.tj", ".gov.tj", ".info.tj", ".int.tj", ".mil.tj", ".museum.tj", ".my.tj", ".name.tj", ".net.tj", ".nic.tj", ".org.tj", ".per.tj", ".pro.tj", ".test.tj", ".web.tj", ".tj",
			".ac.th", ".co.th", ".go.th", ".mi.th", ".or.th", ".net.th", ".in.th", ".th",
			".tl",
			".tm",
			".co.ae", ".net.ae", ".gov.ae", ".ac.ae", ".sch.ae", ".org.ae", ".mil.ae", ".pro.ae", ".name.ae", ".ae",
			".uz",
			".com.vn", ".biz.vn", ".edu.vn", ".gov.vn", ".net.vn", ".org.vn", ".int.vn", ".ac.vn", ".pro.vn", ".info.vn", ".health.vn", ".name.vn", ".vn",
			".com.ye", ".co.ye", ".ltd.ye", ".me.ye", ".net.ye", ".org.ye", ".plc.ye", ".gov.ye", ".ye",
			".edu.tw", ".gov.tw", ".mil.tw", ".com.tw", ".net.tw", ".org.tw", ".idv.tw", ".game.tw", ".ebiz.tw", ".club.tw", ".tw",
			//Africa
			//http://en.wikipedia.org/wiki/List_of_sovereign_states_and_dependent_territories_in_Africa
			".com.dz", ".org.dz", ".net.dz", ".gov.dz", ".edu.dz", ".asso.dz", ".pol.dz", ".art.dz", ".dz",
			".ed.ao", ".gv.ao", ".og.ao", ".co.ao", ".it.ao", ".ao",
			".gouv.bj", ".mil.bj", ".edu.bj", ".gov.bj", ".asso.bj", ".barreau.bj", ".com.bj", ".bj",
			".co.bw", ".org.bw", ".bw",
			".bf",
			".bi",
			".com.cm", ".co.cm", ".net.cm", ".cm",
			".net.cv", ".gov.cv", ".org.cv", ".edu.cv", ".int.cv", ".publ.cv", ".com.cv", ".nome.cv", ".cv",
			".cf",
			".td",
			".com.km", ".coop.km", ".asso.km", ".nom.km", ".presse.km", ".tm.km", ".medecin.km", ".notaires.km", ".pharmaciens.km", ".veterinaire.km", ".edu.km", ".gouv.km", ".mil.km", ".km",
			".com.cd", ".net.cd", ".org.cd", ".cd",
			".org.ci", ".or.ci", ".com.ci", ".co.ci", ".edu.ci", ".ed.ci", ".ac.ci", ".net.ci", ".go.ci", ".asso.ci", ".aeroport.ci", ".int.ci", ".presse.ci", ".ci",
			".cg",
			".dj", ".com.eg", ".edu.eg", ".eun.eg", ".gov.eg", ".info.eg", ".mil.eg", ".name.eg", ".net.eg", ".org.eg", ".sci.eg", ".tv.eg", ".eg",
			".gq",
			".com.er", ".edu.er", ".gov.er", ".mil.er", ".net.er", ".org.er", ".ind.er", ".er",
			".com.et", ".gov.et", ".org.et", ".edu.et", ".net.et", ".biz.et", ".name.et", ".info.et", ".et",
			".md.ga", ".presse.ga", ".gouv.ga", ".go.ga", ".org.ga", ".or.ga", ".com.ga", ".co.ga", ".edu.ga", ".ed.ga", ".ac.ga", ".net.ga", ".aeroport.ga", ".int.ga", ".ga",
			".gm",
			".com.gh", ".edu.gh", ".gov.gh", ".org.gh", ".mil.gh", ".gh",
			".com.gn", ".ac.gn", ".gov.gn", ".org.gn", ".net.gn", ".gn",
			".gw",
			".co.ke", ".or.ke", ".ne.ke", ".go.ke", ".ac.ke", ".sc.ke", ".me.ke", ".mobi.ke", ".info.ke", ".co.ke", ".ke",
			".ac.ls", ".co.ls", ".gov.ls", ".net.ls", ".org.ls", ".parliament.ls", ".ls",
			".com.lr", ".edu.lr", ".gov.lr", ".org.lr", ".net.lr", ".lr",
			".com.ly", ".net.ly", ".gov.ly", ".plc.ly", ".edu.ly", ".sch.ly", ".med.ly", ".org.ly", ".id.ly", ".ly",
			".org.mg", ".nom.mg", ".gov.mg", ".prd.mg", ".tm.mg", ".edu.mg", ".mil.mg", ".com.mg", ".in.mg", ".mg",
			".ac.mw", ".co.mw", ".com.mw", ".coop.mw", ".edu.mw", ".gov.mw", ".int.mw", ".museum.mw", ".net.mw", ".org.mw", ".mw",
			".com.ml", ".net.ml", ".org.ml", ".edu.ml", ".gov.ml", ".presse.ml", ".ml",
			".gov.mr", ".mr",
			".com.mu", ".net.mu", ".org.mu", ".gov.mu", ".ac.mu", ".co.mu", ".or.mu", ".mu",
			".net.ma", ".ac.ma", ".org.ma", ".gov.ma", ".press.ma", ".co.ma", ".ma",
			".adv.mz", ".ac.mz", ".co.mz", ".org.mz", ".gov.mz", ".edu.mz", ".mz",
			".co.na", ".com.na", ".na",
			".ne",
			".com.ng", ".org.ng", ".gov.ng", ".edu.ng", ".net.ng", ".sch.ng", ".name.ng", ".mobi.ng", ".mil.ng", ".ng",
			".gov.rw", ".net.rw", ".edu.rw", ".ac.rw", ".com.rw", ".co.rw", ".int.rw", ".mil.rw", ".gouv.rw", ".rw",
			".nic.st", ".gov.st", ".saotome.st", ".principe.st", ".consulado.st", ".embaixada.st", ".org.st", ".edu.st", ".net.st", ".com.st", ".store.st", ".mil.st", ".co.st", ".st",
			".art.sn", ".com.sn", ".edu.sn", ".gouv.sn", ".org.sn", ".perso.sn", ".univ.sn", ".sn",
			".com.sc", ".net.sc", ".edu.sc", ".gov.sc", ".org.sc", ".sc",
			".com.sl", ".net.sl", ".org.sl", ".edu.sl", ".gov.sl", ".sl",
			".so",
			".ac.za", ".city.za", ".co.za", ".edu.za", ".gov.za", ".law.za", ".mil.za", ".nom.za", ".org.za", ".ecape.school.za", ".fs.school.za", ".gp.school.za", ".kzn.school.za", ".mpm.za", ".ncape.school.za", ".lp.school.za", ".nw.school.za", ".wcape.school.za", ".alt.za", ".net.work.za", ".ngo.za", ".tm.za", ".web.za", ".bourse.za", ".agric.za", ".cybernet.za", ".grondar.za", ".iaccess.za", ".inca.za", ".nis.za", ".olivetti.za", ".pix.za",
			".ss",
			".com.sd", ".net.sd", ".org.sd", ".edu.sd", ".med.sd", ".tv.sd", ".gov.sd", ".info.sd", ".sd",
			".co.sz", ".ac.sz", ".org.sz", ".sz",
			".co.tz", ".ac.tz", ".go.tz", ".or.tz", ".mil.tz", ".sc.tz", ".ne.tz", ".hotel.tz", ".mobi.tz", ".tv.tz", ".info.tz", ".me.tz", ".tz",
			".tg",
			".com.tn", ".ens.tn", ".fin.tn", ".gov.tn", ".ind.tn", ".intl.tn", ".nat.tn", ".net.tn", ".org.tn", ".info.tn", ".perso.tn", ".tourism.tn", ".edunet.tn", ".rnrt.tn", ".rns.tn", ".rnu.tn", ".mincom.tn", ".agrinet.tn", ".defense.tn", ".tn",
			".co.tt", ".com.tt", ".org.tt", ".net.tt", ".biz.tt", ".info.tt", ".pro.tt", ".int.tt", ".coop.tt", ".jobs.tt", ".mobi.tt", ".travel.tt", ".museum.tt", ".aero.tt", ".cat.tt", ".tel.tt", ".tt",
			".asso.re", ".nom.re", ".com.re", ".re",
			".co.ug", ".ac.ug", ".sc.ug", ".go.ug", ".ne.ug", ".or.ug", ".org.ug", ".com.ug", ".ug",
			".ac.zm", ".co.zm", ".com.zm", ".edu.zm", ".gov.zm", ".net.zm", ".org.zm", ".sch.zm", ".zm",
			".ac.zw", ".co.zw", ".zw",
			//Generic TLD's
			".academy", ".accountants", ".actor", ".aero", ".agency", ".associates", ".audio",
			".bar", ".bargains", ".beer", ".bid", ".bike", ".bio", ".biz", ".black", ".blackfriday", ".blue", ".boutique", ".build", ".builders", ".buzz",
			".cab", ".camera", ".camp", ".capital", ".cards", ".care", ".careers", ".cash", ".catering", ".center", ".ceo", ".cheap", ".christmas", ".church", ".claims", ".cleaning", ".clinic", ".clothing", ".club", ".codes", ".coffee", ".college", ".community", ".company", ".computer", ".condos", ".construction", ".consulting", ".contractors", ".cooking", ".cool", ".coop", ".country", ".credit", ".creditcard", ".cruises",
			".dance", ".date", ".dating", ".deals", ".democrat", ".dental", ".diamonds", ".digital", ".direct", ".directory", ".discount", ".domains",
			".education", ".email", ".engineering", ".enterprises", ".equipment", ".estate", ".events", ".exchange", ".expert", ".exposed",
			".fail", ".farm", ".finance", ".financial", ".fish", ".fishing", ".fitness", ".flights", ".florist", ".foo", ".foundation", ".fund", ".furniture", ".futbol",
			".gallery", ".gift", ".glass", ".global", ".gop", ".graphics", ".gratis", ".gripe", ".guide", ".guitars", ".guru",
			".haus", ".hiphop", ".hiv", ".holdings", ".holiday", ".horse", ".host", ".house",
			".immobilien", ".industries", ".info", ".ink", ".institute", ".insure", ".international", ".investments",
			".jetzt", ".jobs", ".juegos",
			".kaufen", ".kitchen", ".kim",
			".land", ".lease", ".life", ".lighting", ".limited", ".limo", ".link", ".loans", ".luxury",
			".maison", ".management", ".marketing", ".media", ".menu", ".mobi", ".moda", ".moe", ".museum",
			".name", ".ninja",
			".onl", ".organic",
			".partners", ".parts", ".photo", ".photography", ".photos", ".pics", ".pictures", ".pink", ".place", ".plumbing", ".post", ".press", ".pro", ".productions", ".products", ".properties", ".pub",
			".recipes", ".red", ".reise", ".reisen", ".ren", ".rentals", ".repair", ".report", ".republican", ".rest", ".reviews", ".rich", ".rocks", ".rodeo",
			".schule", ".services", ".sexy", ".shiksha", ".shoes", ".singles", ".social", ".solar", ".solutions", ".space", ".supplies", ".supply", ".support", ".surf", ".surgery", ".systems",
			".tattoo", ".tax", ".technology", ".tel", ".tienda", ".tips", ".today", ".tools", ".town", ".toys", ".trade", ".training", ".travel",
			".university", ".uno",
			".vacations", ".versicherung", ".ventures", ".viajes", ".villas", ".vision", ".vodka", ".voting", ".voyage",
			".wang", ".watch", ".webcam", ".website", ".wed", ".wiki", ".works", ".wtf",
			".xxx",
			".xyz",
			".zone",
			".asia", ".bayern", ".berlin", ".brussels", ".bzh", ".capetown", ".cat", ".cologne", ".durban", ".eus", ".frl", ".gal", ".hamburg", ".joburg", ".kiwi", ".koeln", ".london", ".melbourne", ".moscow", ".nagoya", ".nyc", ".paris", ".quebec", ".ruhr", ".scot", ".tokyo", ".vegas", ".vlaanderen", ".wien", ".yokohama",
			//International
			".biz",
			".cc",
			".com",
			".int",
			".io",
			".net",
			".org",
			".eu",
			".gov",
			".mil",
			".edu",
			".net",
			".org",
			".info",
			".nu",
			".tk"
		);

		//Clean up input and lower case hostname
		$url = strtolower(trim($url));

		// Check if http or https for parse_url
		if (strpos($url, 'http://') === FALSE && strpos($url, 'https://') === FALSE) {
			$url = 'http://' . $url;
		}

		//Now explode the URL and explode the hostname
		$urlparts = parse_url($url);

		if (isset($urlparts['host'])) {
			$hostname = $urlparts['host'];

			//Check if we support this TLD
			$tld = "";
			for ($x = 0; $x < count($tlds); $x++) {
				if ($this->endsWith($hostname, $tlds[$x])) {
					$tld = $tlds[$x];
					break;
				}
			}

			//Only continue if we know the TLD
			if (strlen($tld) > 0) {

				//Get all the charaters that are in front of the TLD
				$prefix = substr($hostname, 0, (strlen($hostname) - (strlen($tld) )));

				//Explode the prefix so we keep subdomain and domain
				$parts = explode(".", $prefix);

				//Rebuild the subdomain
				$subdomain = "";
				for ($x = 0; $x < (count($parts) - 1); $x++) {
					if ($subdomain === "") {
						$subdomain = $parts[$x];
					} else {
						$subdomain.= "." . $parts[$x];
					}
				}

				//Create array
				$result = array();
				$result['tld'] = $tld;
				$result['domain'] = $parts[(count($parts) - 1)] . $tld;
				$result['subdomain'] = $subdomain;
				$result['hostname'] = $hostname;
				if (strlen($subdomain) > 0) {
					$result['url'] = $result['subdomain'] . "." . $result['domain'];
				} else {
					$result['url'] = $result['domain'];
				}
				return $result;
			}
		}

		return false;
	}
}
