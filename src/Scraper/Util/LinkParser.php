<?php

namespace Scraper\Util;

use Monolog\Logger;
use Doctrine\DBAL\Connection;
use Scraper\Util\Uri;

/**
 * Description of Parser
 *
 * @author Kevin
 */
class LinkParser
{
	/**
	 * @var Logger
	 */
	protected $log;
	
	/**
	 * @var Connection 
	 */
	protected $db;
	
	protected $uri;
	
	protected $url = "";
	
	protected $hostname = "";
	
	protected $document = "";
	
	protected $parsed = 0;
	
	protected $time = 0;
	
	protected $links = array();
	
	/**
	 * 
	 * @param \Monolog\Logger $log
	 * @param \Doctrine\DBAL\Connection $db
	 * @param string $url
	 * @param string $document
	 */
	public function __construct(Logger $log, Connection $db, $url, $document) {
		
		$this->log = $log;
		$this->db = $db;
		
		$this->uri = new Uri();
		$this->url = $url;
		
		$this->hostname = $this->uri->getHostName($url);
		$this->document = $document;
		$this->parsed = $this->_parse($document);
	}
	
	private function _parse($document){
		
		//Parse document and keep track of time
		$start = microtime(true);
		
		$document = str_replace("'", "\"", $document);
		
		//USe regex to retrieve links
		$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
		if(preg_match_all("/$regexp/siU", $document, $matches, PREG_SET_ORDER)) {
			foreach($matches as $match) {
				$this->_processURL($match[2], $match[3]);
			}		
		}
				
		$this->time = ((microtime(true) - $start) * 1000);

		return true;
	}
	
	private function _processURL($ahref, $anchor){
	
		$valid = true;
		if(strpos($ahref, 'javascript:')>-1 || strpos($ahref, 'mailto:')>-1){
			$valid = false;
		}

		//volgen we nog niet
		if(strpos($ahref, '../')>-1){
			$valid = false;
		}		
		
		if($valid == true){
			
			//Remove querie (domain?querie)
			if (strpos($ahref, "#") > -1) {
				$ahref = substr($ahref, 0, strpos($ahref, "#"));
			}
			
			if(substr($ahref, 0, 7) != "http://" && substr($ahref, 0, 8) != "https://"){
				if(strpos($this->url, "https://") == false){
					$hostname = "http://".$this->hostname;
				} else {
					$hostname = "https://".$this->hostname;
				}
				if(substr($ahref, 0, 1) == "/"){
					$ahref = $hostname.$ahref;
				} else {
					$ahref = $hostname."/".$ahref;
				}
			}

			//Only add unique links
			if(array_key_exists($ahref, $this->links) === false){
				$this->links[$ahref][] = $this->_cleanAnchor($anchor);
			}
		}
		
		return true;
	}
	
	private function _cleanAnchor($anchor){
		
		if(strpos($anchor, "<img")>-1){
			$anchor = "[image]";
		}elseif(strlen($anchor)>256){
			$anchor = "[block]";
		} else {
			$anchor = trim(strip_tags(substr($anchor, 0, 256)));
			if($anchor == ""){
				$anchor = "[empty]";
			} else {
				$anchor = $this->_removeLineBreaks($anchor);
			}
		}
	
		return $anchor;
	}
	
	private function _removeLineBreaks($value){
		
		$value = str_replace(array("\r\n", "\r"), "\n", $value);
		$lines = explode("\n", $value);
		
		$result = array();
		foreach ($lines as $i => $line) {
			if(!empty($line)){
				$result[] = trim($line);
			}
		}
		
		return implode($result);		
	}
	
	public function getLinks(){
		return $this->links;
	}	
	
}
