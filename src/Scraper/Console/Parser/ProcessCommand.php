<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Scraper\Console\Parser;

use Framework\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Monolog\Logger;
use Scraper\Console\Parser\Process;

/**
 * Description of DemoCommand
 *
 * @author Kevin
 */
class ProcessCommand extends Command {

    /**
     * @var Framework\Application;
     */
    protected $app;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * Constructor for initializiing the processor.
     * @param Application $app
     * @param Logger $logger
     * @param type $name
     */
    public function __construct(Application $app, Logger $logger, $name = null) {
	parent::__construct($name);
	$this->app = $app;
	$this->logger = $logger;
    }

    /**
     * Configuration for the processor command.
     */
    protected function configure() {
	$this
		->setName('scraper:process')
		->setDescription('Process scraped data.')
		->addArgument('method', \Symfony\Component\Console\Input\InputArgument::OPTIONAL, 'RUN|SCHEDULE|REPROCESS|RESTORE', 'SCHEDULE')
		->addOption('projectid', 'projectid', \Symfony\Component\Console\Input\InputOption::VALUE_OPTIONAL, 'Project id', null);
    }

    /**
     * Executor of the processor command
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
	$this->input = $input;
	$this->output = $output;

	// Check if we need to schedule
	$logger = $this->logger;
	$clsProcess = new Process($this->app, $this->app->getDatabaseConnection(), $logger);

	/**
	 * checks wich action should be performed.
	 */
	switch (strtoupper($input->getArgument('method'))) {
	    case "RUN":
		try {
		    $projectid = $this->checkProjectId();
		    $clsProcess->run($projectid);
		} catch (\Exception $e) {
		    // cleaning up thread database if an error occured.
		    $this->app->getDb()->delete('thread', array('id' => getmypid()));
		    $this->app->getDb()->update('urls', array('pid' => 0), array('pid' => getmypid()));
		    $logger->addError($e->getMessage(), array($e->getTrace()));
		}
		break;
	    
	    case "SCHEDULE":
		$clsProcess->schedule();
		break;
	    
	    case "REPROCESS":
		try {
		    $projectid = $this->checkProjectId();
		    $logger->addDebug('PREPARING REPROCESS', array($this->app->getDb()->update('movies', array('processed' => 0), array('project' => $projectid))));
		    $clsProcess->schedule();
		} catch (\Exception $e) {
		    // cleaning up thread database if an error occured.
		    $this->app->getDb()->delete('thread', array('id' => getmypid()));
		    $this->app->getDb()->update('urls', array('pid' => 0), array('pid' => getmypid()));
		    $logger->addError($e->getMessage(), array($e->getTrace()));
		}
		break;
	    
	    case "RESTORE":
		try {
		    $projectid = $this->checkProjectId();
		    $this->app->getDb()->update('movies', array('processed' => 0, 'pid' => 0), array('project' => $projectid));
		} catch (\Exception $e) {
		    // cleaning up thread database if an error occured.
		    $logger->addError($e->getMessage(), array($e->getTrace()));
		}
		break;
	    default:

		break;
	}
    }

    /**
     * 
     * @return type
     */
    protected function checkProjectId() {

	$projectid = $this->input->getOption('projectid');

	if (!( $projectid > 0 )) {
	    $question = new Question('No valid projectid has been found, what project id should be used?', 0);
	    $projectid = $helper->ask($this->input, $this->output, $question);
	    $this->checkProjectId();
	}
	return $projectid;
    }

}
