<?php

namespace Scraper\Console\Parser;

use Monolog\Logger;
use Doctrine\DBAL\Connection;
use Framework\Application;
use Scraper\Model\Thread;


/**
 * Description of Process
 *
 * @author Kevin
 */
class Process
{
	/**
	 *
	 * @var Logger 
	 */
	private $log;
	
	/**
	 * @var Connection 
	 */
	private $db;
	
	/**
	 * @var Application
	 */
	protected $app;
	
	/**
	 * @var mixed
	 */
	private $pid;
	
	/**
	 * Construct of processor
	 * @param \Application\Application $app
	 * @param \Doctrine\DBAL\Connection $db
	 * @param \Monolog\Logger $log
	 */
	public function __construct(Application $app, Connection $db, Logger $log) {
		$this->log = $log;
		
		$this->db = $db;
		
		$this->app = $app;
		
		$this->pid = getmypid();
	}
	
	public function findProjectsToProcess() {
		return $this->db->fetchAll('SELECT DISTINCT project.* FROM movies INNER JOIN project ON project.id=movies.project WHERE movies.processed=0');
	}
	
	public function findActiveThreads($action, $project) {
	    return $this->db->fetchAll('SELECT * FROM thread WHERE project=? AND action=?', array($project, $action));
	}

	/**
	 * Schedule new processes
	 */
	public function schedule() {
		
		// Get projects that needs process
		$active_projects = $this->findProjectsToProcess();
		$newThreadCounter = 0;
		$availableThreadCounter = 0;
		
		if ($active_projects) {

			// Loop the project
			$threadCounter = 0;
			foreach ($active_projects as $project) {

				$threads = (int) $project['settings_processing_threads'];
				$this->log->addInfo('currently running threads', array($threads));

				// Start some threads if needed
				if ($threads != 0) {
					
					$active_threads = $this->findActiveThreads(Thread::ACTION_PROCESS, $project['id']);
					$this->log->addDebug("active threads " , $active_threads);
					if ($active_threads === FALSE || $threads > count($active_threads)) {
						// Loop to schedule
						$add = $threads - ($active_threads ? count($active_threads) : 0);
						$availableThreadCounter = $availableThreadCounter + count($active_threads);
						for ($index = 0; $index < $add; $index++) {
							$newThreadCounter++;
							$execute = exec("php " . $_SERVER['SCRIPT_FILENAME'] . " scraper:process run --projectid=" . (int) $project['id'] . " > /dev/null 2>&1 &", $output, $return);
						}
					}
					
					$this->log->addInfo('threads started', array( 'running threads' => $active_threads, 'new threads' => $newThreadCounter, 'max threads' => $threads));
				}
			}
		}
		
		$this->log->addDebug($this->pid . " - " . $newThreadCounter ." threads started, " . $availableThreadCounter . " threads already running.");
	}

	/**
	 * Run a new processor thread.
	 * @param mixed $projectId
	 */
	public function run($projectId) {
		// Retreive project from the database
		$project = $this->db->fetchAssoc('SELECT * FROM project WHERE id=?', array($projectId));
		
		if ($project) {
			$this->log->addNotice($this->pid . " - Starting processing thread for " . $project['name'] . " with processid ");
		
 			// Add this process
			$this->db->executeQuery('INSERT INTO thread (`id`, `type`, `update`, `project`, `action`, `status`) VALUES (?, ?, NOW(), ?, ?, ?)', array(
			    $this->pid, 'process', $project['id'], Thread::ACTION_DOWNLOAD, 0
			));

			// Set the pid to process
			$statement = $this->db->executeQuery('UPDATE `movies` SET `pid` = ? WHERE `processed` = 0 AND `pid` = 0 AND `project` = ? LIMIT 50', array($this->pid, $project['id']));
			$update_rows = $statement->rowCount();

			try{
				// Run the included class
				$class = 'Scraper\Console\Parser\Processors\\' . $project['processor'] . 'Processor';
				if(class_exists($class)) {
					$this->log->addDebug($this->pid . " - start processing with " . $class);
					$processor = new $class($this->app, $this->db, $this->log);
					$processor->run($project['id']);
				}
			
			} catch(\Exception $ex) {
				$this->log->addDebug('An error occured: ' . $ex->getMessage(), $ex->getTrace());
			}
			
			// This thread is done
			$this->db->delete('thread', array('id' => $this->pid));
			$this->log->addInfo($this->pid . " - Deleting thread", array($update_rows));
			
			// Check if we need to start some
			if ($update_rows > 0) {
				$this->schedule();
			}
		}
	}

}
