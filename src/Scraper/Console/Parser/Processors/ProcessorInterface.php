<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Scraper\Console\Parser\Processors;

use Doctrine\DBAL\Connection;
use Monolog\Logger;
use Framework\Application;

/**
 *
 * @author Kevin
 */
interface ProcessorInterface
{	
	public function __construct(Application $app, Connection $db, Logger $log);
	
	public function scheduleProcess($project_id, $url_id, $url);
	
	public function run($project_id);
	
	public function process($project_id, $url_id, $url);
}
