<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Scraper\Console\Parser\Processors;

use Symfony\Component\DomCrawler\Crawler;
/**
 * Description of ImdbProcessor
 *
 * @author kevinpostma
 */
class ImdbProcessor extends AbstractProcessor {

    public function process($project_id, $url_id, $url, $movieId = null) {
	$this->log->addInfo('MOVIE PROCESSING');

	$reprocess = false;
	$rowCount = 0;
	if ($movieId > 0) {
	    $reprocess = true;
	} else {
	    $query = $this->db->executeQuery('INSERT INTO movies (id, project, created_at) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE id=id ', array($url_id, $project_id, date('Y-m-d H:i:s')));
	    $rowCount = $query->rowCount();
	}

	$this->log->addInfo('ROWCOUNT', array($rowCount, $reprocess));
	if ($rowCount == 1 || $reprocess) {

	    if ($reprocess) {
		$movieId = $movieId;
	    } else {
		$movieId = $this->db->lastInsertId();
	    }

	    $folder = __DIR__ . "/../../../../../app/files/scraper/download/" . $project_id . "/";
	    $page_file = $folder . $url_id . '.html';
	    
	    $this->log->addInfo('FILE_EXISTS', array($page_file => file_exists($page_file)));
	    if (file_exists($page_file)) {
		$this->log->addDebug("processing data from", array($url));
		$movie = new \Movies\Models\Mongodb\Movie();
		$movie->setId($movieId);
		$this->log->addDebug("Parsing Movie class", array($url));
		
		if($movie = $this->processMovie($page_file, $coverage, $movie, $url)) {
		    $this->log->addDebug('Movie done', array());
		    if($movie->isValid()) {
			$this->saveMovie($movie, $reprocess);
			return true;
		    }
		    sleep(2);
		} else {
		    $this->log->addDebug("MOVIE COULD NOT BE PROCESSSED");
		}
	    }
	}
	return false;
    }

    private function processMovie($filename, &$coverage = 0, \Movies\Models\Mongodb\Movie $movie, $url) {

	$crawler = new Crawler();
	$fileContents = file_get_contents($filename);
	$fileContents = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $fileContents);
	
	$crawler->addHtmlContent($fileContents, 'UTF-8');
	$crawler->html();
	
	
	
	$this->log->addDebug('PROCESSING START');
	if ($crawler->valid()) {
	  
	    
	    $this->log->addDebug('PROCESSING TITLE');
	    $header = $crawler->filter('h1');
	    $title = $crawler->filter('h1.header span.itemprop');
	    if($title->count() > 0) {
		$movie->setTitle($title->text());
	    }
	    
	    $this->log->addDebug('PROCESSING YEAR');
	    $year = $crawler->filter('h1.header span.nobr');
	    if($year->count() > 0) {
		$movie->setYear((int)str_replace(array("(",")"), array("",""), $year->text()));
	    }
	    
	    
	    $this->log->addDebug('PROCESSING RATING');
	    $rating = $crawler->filterXPath("//div[@class='star-box-details']/strong/span");
	    if($rating->count() > 0) {
		$movie->setRating((int)str_replace(",", ".", $rating->text()));
	    }

	    $this->log->addDebug('PROCESSING COUNTRY');
	    $country = $crawler->filterXPath("//div[@id='titleDetails']/div[@class='txt-block'][2]/a");
	    if($country->count() > 0) {
		$movie->setCountry($country->text());
	    }

	    $this->log->addDebug('PROCESSING DURATION');
	    $duration = $crawler->filterXPath("//div[@id='titleDetails']/div[@class='txt-block'][9]/time");
	    if($duration->count() > 0) {
		$duration = $duration->text();
		$duration = preg_replace('/\D/', '', $duration);
		$movie->setDuration($duration);
	    }
	    
	    $this->log->addDebug('PROCESSING DESCRIPTION');
	    $description = $crawler->filterXPath('//div[@itemprop="description"]');
	    if($description->count() > 0) {
		$movie->setDescription($description->text());
	    }
	    
	    $this->log->addDebug('PROCESSING CAST');
	    $cast = array();
	    $castCrawler = $crawler->filterXPath('//table[@class="cast_list"]/tr[@class="odd" or @class="even"]/td[@itemprop="actor"]');
	    $this->log->addDebug("CAST COUNT:" . $castCrawler->count());
	    if($castCrawler->count() > 0) {
		$castCrawler->each(function(Crawler $node, $index) use(&$cast) {
		    $artist = $this->dm->getRepository('\Movies\Models\Mongodb\Artist')->findOneBy(array('name' => trim($node->text())));
		    if(!$artist){
			$artist = new \Movies\Models\Mongodb\Artist();
			$artist->setName(trim($node->text()));
		    }
		    $cast[] = $artist;
		});
	    }
	    $movie->setCast($cast);
	    
	    $this->log->addDebug('PROCESSING DIRECTORS');
	    $directors = array();
	    $directorsCrawler = $crawler->filterXPath('//div[@itemprop="director"]/a/span[@itemprop="name"]');
	    if($directorsCrawler->count() > 0) {
		$directorsCrawler->each(function(Crawler $node, $index) use(&$directors)  {
		    $director = $this->dm->getRepository('\Movies\Models\Mongodb\Director')->findOneBy(array('name' => trim($node->text())));
		    if(!$director){
			$director = new \Movies\Models\Mongodb\Director();
			$director->setName(trim($node->text()));
		    }
		    $directors[] = $director;
		});
	    }
	    $movie->setDirectors($directors);
	    
	    $this->log->addDebug('PROCESSING GENRES');
	    $genres = array();
	    $genresCrawler = $crawler->filterXPath('//div[@itemprop="genre"]/a');
	    if($genresCrawler->count() > 0) {
		$genresCrawler->each(function(Crawler $node, $index) use(&$genres)  {
		    $genre = $this->dm->getRepository('\Movies\Models\Mongodb\Genre')->findOneBy(array('genre' => trim($node->text())));
		    if(!$genre){
			$genre = new \Movies\Models\Mongodb\Genre();
			$genre->setGenre(trim($node->text()));
		    }
		    $genres[] = $genre;
		});
	    }
	    $movie->setGenres($genres);
	    
	    $this->log->addDebug('PROCESSING WRITERS');
	    $writers = array();
	    $writersCrawler = $crawler->filterXPath('//div[@itemprop="creator"]/a[@itemprop="url"]');
	    if($writersCrawler->count() > 0) {
		$writersCrawler->each(function(Crawler $node, $index) use(&$writers)  {
		    $writer = $this->dm->getRepository('\Movies\Models\Mongodb\Director')->findOneBy(array('name' => trim($node->text())));
		    if(!$writer){
			$writer = new \Movies\Models\Mongodb\Director();
			$writer->setName(trim($node->text()));
		    }
		    $writers[] = $writer;
		});
	    }
	    $movie->setWriters($writers);
	    
	    
	    $this->log->addDebug('PROCESSING CHECK FOR POSSIBLE ERRORS');
	    if(is_null($movie->getTitle()) || is_null($movie->getYear()) || empty($movie->getTitle()) || empty($movie->getYear())) {
		$this->db->update('urls', array('http_code' => 666), array('id' => $movie->getId()));
	    }

	    /*	     * ************************************************* EIND */
	    $this->log->addDebug('PROCESSING END', array(
		'title' => $movie->getTitle(),
		'year'	=> $movie->getYear(),
		'description' => $movie->getDescription(),
		'cast' => $movie->getCast(),
		'country' => $movie->getCountry(),
		'directors' => $movie->getDirectors(),
		'writers' => $movie->getWriters(),
		'genres' => $movie->getGenres(),
	    ));
	    
	    $crawler->clear();
	    unset($crawler);

	    return $movie;
	} else {
	    $crawler->clear();
	    unset($crawler);
	}
	return false;
    }

}
