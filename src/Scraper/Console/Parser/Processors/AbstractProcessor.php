<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Scraper\Console\Parser\Processors;

use Doctrine\DBAL\Connection;
use Monolog\Logger;
use Framework\Application;
use Scraper\Util\Uri;

use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * Description of AbstractProcessor
 *
 * @author Kevin
 */
abstract class AbstractProcessor implements ProcessorInterface
{
	/**
	 * @var Connection
	 */
	protected $db;
	
	/**
	 * @var Logger
	 */
	protected $log;
	
	/**
	 * @var Application
	 */
	protected $app;
	
	/**
	 *
	 * @var type 
	 */
	protected $pid;
	
	/**
	 * @var DocumentManager 
	 */
	protected $dm;
	
	/**
	 * 
	 * @param \Doctrine\DBAL\Connection $db
	 * @param \Monolog\Logger $Log
	 */		
	public function __construct(Application $app, Connection $db, Logger $log)
	{
		$this->log = $log;
		$this->db = $db;
		$this->app = $app;
		$this->pid = getmypid();
		$this->dm = $app->getDocumentManager();
	}
	
	/**
	 * Run a processing thread to process a batch of urls registered to this process id.
	 * @param type $project_id
	 */
	public function run($project_id) {

		// get process id.
		$pid = getmypid();

		// loop through the results
		$resultsCount = 0;
		while ($results =  $this->db->fetchAll('SELECT movies.id AS id, movies.id AS url_id, urls.project AS project, urls.url AS url FROM `movies` INNER JOIN urls ON urls.id = movies.id WHERE movies.`pid` = ?' , array( (int) $pid) )  ) {
			
			foreach ($results as $result) {
				$resultsCount++;
				$this->log->addInfo("PROCESSING WITH PROCESSOR", array(get_class($this), $result));
				if ($this->process($result['project'], $result['url_id'], $result['url'], $result['id'])) {
					$this->log->addInfo("PID-" . $pid . ": Succesfull processed " , array($result['url_id'], $result['url']));
					$updated = $this->db->executeQuery("UPDATE `movies` SET `pid` = 0, `updated_at` = NOW(), `processed` = 1 WHERE `id` = ?", array($result['url_id']));
				} else {
					$this->log->addInfo("PID-" . $pid . ": No good process " . $result['url_id']);
					$updated = $this->db->executeQuery("UPDATE `movies` SET `pid` = 0, `updated_at` = NOW(), `processed` = 1, `invalid`=1 WHERE `id` = ?", array($result['url_id']));
				}
				
			}
		} 
		
		// log if no results could be retreived.
		if($resultsCount == 0) {
			$this->log->addNotice('NO RESULTS RETURNED');
		}
	}
	
	/**
	 * Scheduling a new process.
	 * this method will be called directly from the scraper. After a page has been downloaded that needs to be processed this method will be called.
	 * this method can process the html page immediatly or you can schedule the url to process the document later on.
	 * 
	 * @param type $project_id
	 * @param type $url_id
	 * @param type $url
	 */
	public function scheduleProcess($project_id, $url_id, $url)
	{
		$rules = $this->db->fetchAll("SELECT * FROM project_rule WHERE project=?", array($project_id));
		foreach($rules as $rule)
		{
			if($rule['processurl'] && preg_match($rule['regex'], $url)) {
				$this->log->addAlert('ISPROCESSURL', array($rule['processurl'], $rule['regex']));
				$this->process($project_id, $url_id, $url);
			}
		}
	}
	
	/**
	 * Clean value.
	 * @param type $value
	 * @return string
	 */
	protected function cleanValue($value) {
		$value = html_entity_decode($value);
		$value = preg_replace('/\s+/', ' ', $value);
		$value = str_replace("\"", "'", $value);
		$value = str_replace(";", ":", $value);

		if (strlen($value) == false) {
			$value = "-";
		}

		return $value;
	}
	
	/**
	 * Stores a movieobject to the mongodb database.
	 * 
	 * @param \Movies\Models\Mongodb\Movie $movie
	 * @param type $exists
	 */
	protected function saveMovie(\Movies\Models\Mongodb\Movie $movie, $exists = false) {
	    $this->log->addDebug('EXISTS (UPDATE MOVIE)', array($exists));
	    try { 
		if($exists) {
		    $this->log->addDebug("MERGING MOVIE");
		    $this->dm->merge($movie);
		} else {
		    $this->log->addDebug("PERSISTING MOVIE");
		    $this->dm->persist($movie);
		}
		$this->log->addDebug("FLUSHING DATABASE");
		$this->dm->flush();
	    } catch (\Exception $ex) {
		$this->log->addDebug($ex->getMessage());
	    }
	    $this->log->addDebug("MOVIE STORED TO MONGODB");
	}
}
