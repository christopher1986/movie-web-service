<?php

namespace Scraper\Console\Scraper;

use Monolog\Logger;
use Doctrine\DBAL\Connection;
use Framework\Application;
use Scraper\Model\Thread;

/**
 * Description of Download
 *
 * @author Kevin
 */
class Download
{	
	const BATCH_SIZE = 50;
	/**
	 *
	 * @var Logger 
	 */
	private $log;
	
	/**
	 * @var Connection 
	 */
	private $db;
	
	/**
	 * @var Application
	 */
	protected $app;
	
	/**
	 * @var mixed
	 */
	private $pid;
	
	/**
	 * 
	 * @param \Application\Application $app
	 * @param \Doctrine\DBAL\Connection $db
	 * @param \Monolog\Logger $log
	 */
	public function __construct(Application $app, Connection $db, Logger $log) {
		$this->log = $log;
		
		$this->db = $db;
		
		$this->app = $app;
		
		$this->pid = getmypid();
	}
	
	public function refresh(array $projectids) {
	    $this->app->getDb()->executeQuery('update urls set pid=?, http_code=? WHERE priority > ? AND http_code=? AND project IN(?)',array(0, 0, 20, 200, $projectids), array(\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \Doctrine\DBAL\Connection::PARAM_INT_ARRAY));
	}
	
	public function findActiveProjects() {
	    return $this->db->fetchAll('SELECT * FROM project WHERE settings_download_threads > 0');
	}
	
	public function findActiveThreads($action, $project) {
	    return $this->db->fetchAll('SELECT * FROM thread WHERE project=? AND action=?', array($project, $action));
	}
	
	/**
	 * Scheduling a download process.
	 */
	public function schedule() {

		$this->log->addNotice($this->pid . " - Scheduling download and starting up threads.");
		// Get projects that needs process
		$active_projects =  $this->findActiveProjects();
		$threadCounter = 0;
		if ($active_projects) {

			// Loop the project
			foreach ($active_projects as $project) {
				$this->log->addNotice($this->pid . " - Scheduling download and starting up threads.", array($project));

				$threads = (int) $project['settings_download_threads'];
				
				$this->initProject($project['id']);	

				// Start some threads if needed
				if ($threads != 0 && $project['id'] > 0) {
					
					$active_threads = $this->findActiveThreads(Thread::ACTION_DOWNLOAD, $project['id']);
					
					if ($active_threads === FALSE || $threads > count($active_threads)) {
						
						// Loop to schedule
						$add = $threads - ($active_threads ? count($active_threads) : 0);
						$this->log->addDebug($this->pid . " - starting threads. ", array($add));
						for ($index = 0; $index < $add; $index++) {
							$threadCounter++;
							$this->log->addDebug($this->pid . " executing: " . "php " . $_SERVER['PWD'] . "/app/console.php scraper:download run --projectid=" . (int) $project['id'] . " > /dev/null 2>&1 &");
							$execute = exec("php " . $_SERVER['SCRIPT_FILENAME'] . " scraper:download run --projectid=" . (int) $project['id'] . " > /dev/null 2>&1 &", $output, $return);
						}
					}
				}
			}
		}
		$this->log->addNotice($this->pid . " - " . $threadCounter . " threads started.");
	}

	/**
	 * Starting up a download thread that will download a batch of urls
	 * @param mixed $projectId
	 */
	public function run($projectids) {
		if(is_array($projectids) && count($projectids) > 0) {
		    $projectId = $projectids[0];
		} else {
		    $projectId = $projectids;
		}
		
		// Get the project
		$project = $this->db->fetchAssoc("SELECT * FROM project WHERE id=?", array($projectId));		
		
		if ($project) {
			$this->log->addNotice($this->pid . " - Starting thread for " . $project['name'] . " with processid ");
		
 			// Add this process
			$this->db->executeQuery('INSERT INTO thread (`id`, `type`, `update`, `project`, `action`, `status`) VALUES (?, ?, NOW(), ?, ?, ?)', array(
			    $this->pid, 'download', $project['id'], Thread::ACTION_DOWNLOAD, 0
			));

			// reserve batch for this process
			$batch_size = $this->reserveBatch($project);
			
			// Run the downloader and process the reserved batch.
			if($batch_size > 0 ){
				$downloader = $this->createDownloaderInstance($project);
				try {
					$this->log->addNotice($this->pid . " - Start running downloader", array(get_class($downloader)));
					$download = $downloader->run();
				} catch(\Exception $ex) {
					$this->db->update('urls', array('pid' => 0), array('pid' => $this->pid));
					$this->db->delete('thread', array('id' => $this->pid));
					$this->log->addCritical("Exception occured, restored pids. error message:" . $ex->getMessage());
					exit();
				}
				
			} else {
				$this->log->addNotice($this->pid . " - could not create new batch. Probably all urls have been downloaded.");
			}
			
			//delete this finished thread. 
			$this->db->delete('thread', array('id' => $this->pid));
			$this->log->addNotice($this->pid . " - finished thread for " . $project['name']);
			
			// Check if we need to start some
			if ($batch_size && $batch_size > 0) {
				$this->schedule();
			}
		}
	}
	
	/**
	 * Reserve a batch with new urls to download.
	 * @return boolean (true if urls are reserved, false if there where no urls to reserve | Project is finished in this case.)
	 */
	public function reserveBatch(array $project) {
		$this->log->addNotice($this->pid . " - create new download batch");
		
		//Check how big the queue is for this PID
		$queuesize = $this->getQueueSize($project['id'], $this->pid);
		
		if ($queuesize == 0) {
			$query = 'SELECT `id` FROM urls WHERE project=? AND pid=? AND priority > 0 AND http_code=? ORDER BY priority DESC LIMIT ' . self::BATCH_SIZE;
			$this->log->addDebug($this->pid . " - reserve batch order by", array($query));
			
			$reserves = $this->db->fetchAll($query, array(
				$project['id'], 0, 0
			));	
			
			//check if there are reserved urls.
			if(count($reserves) > 0) {
				// get reserve id's
				$reserve_ids = array();
				foreach ($reserves as $id) {
					$reserve_ids[] = $id['id'];
				}

				//update urls set pid to current process
				$reserve = $this->db->executeQuery('UPDATE urls SET pid=? WHERE id IN(?)', array( $this->pid, $reserve_ids),
					array(\PDO::PARAM_INT,\Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
				);

				//check if batch was created and return batch size.
				if ($reserve !== false) {
					$this->log->addNotice($this->pid . " - new batch created. " . $reserve->rowCount() . " urls are ready to download for process ");
					return $reserve->rowCount();
				}
			}
		}
		return false;
	}
	
	/**
	 * Returns the current Queuesize of this thread.
	 * @param type $projectid
	 * @param type $pid
	 * @return type
	 */
	private function getQueueSize($projectid, $pid) {
	    return $this->db->fetchColumn('SELECT count(*) AS total FROM urls WHERE project=? AND pid=? AND http_code=?', array(
		    $projectid, $pid, 0
	    ), 0);
	}

	/**
	 * Check if project already has been analysed.
	 * @param mixed $project_id
	 */
	public function initProject($project_id)
	{
		$urls = $this->db->fetchColumn('SELECT count(id) as total FROM urls WHERE project=?', array($project_id));
		$this->log->addDebug($this->pid . " -  " . $urls . " existing urls for this project");
		if($urls == 0) {
			$url = $this->db->fetchColumn('SELECT startingpoint FROM project WHERE id=?', array($project_id));
			$this->db->insert('urls', array(
				'url' => $url,
				'pid' => 0,
				'project' => $project_id,
				'priority' => 100,
				'http_code' => 0,
				'size_download' => 0,
				'incoming_links' => 0,
				'content_hash' => null,
				'created_at' => date("Y-m-d"),
			));
		}
	}
	
	/**
	 * @return Downloader\Downloader
	 * @param Entities\Project
	 */
	private function createDownloaderInstance($project)
	{
	    
		$customDownloader = '\Scraper\Console\Scraper\Downloader\Project\Downloader' . $project['id'];
		if(class_exists($customDownloader)) {
			$downloader = new $customDownloader($project, $this->log, $this->db, $this->app);
		} else {
			$downloader = new \Scraper\Console\Scraper\Downloader\Downloader($project, $this->log, $this->db, $this->app);
		}
		$this->log->addDebug('Downloader instance created', array('Downloader' => $downloader, 'is object' => is_object($downloader), 'class methods' => get_class_methods(get_class($downloader))));
		return $downloader;
	}
}
