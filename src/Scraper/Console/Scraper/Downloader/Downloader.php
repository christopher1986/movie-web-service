<?php

namespace Scraper\Console\Scraper\Downloader;

use Monolog\Logger;
use Doctrine\DBAL\Connection;
use Framework\Application;
use Scraper\Model\Thread;
use Scraper\Util\Uri;
use Scraper\Util\LinkParser;
/**
 * Description of Download
 *
 * @author kevinpostma
 */
class Downloader {
    
	const STATUS_RATE_LIMIT_EXCEEDED = 1;
	const STATUS_SCANNING = 0;
	
	/**
	 * @var Application
	 */
	protected $app;
	
	/**
	 * @var Logger
	 */
	protected $log;
	
	/**
	 * @var Connection 
	 */
	protected $db;
	
	/**
	 * @var Array
	 */
	protected $project;
	
	/**
	 * @var type 
	 */
	protected $navigator;
	
	/**
	 * @var Commands\Scraper\Lib\Processor\AbstractProcessor
	 */
	protected $processor;
	
	/**
	 * @var integer
	 */
	protected $pid = 0;
	
	/**
	 * @var \Entities\Thread
	 */
	protected $thread;

	/**
	 * Downloader constructor
	 * @param \Entities\Project $project
	 * @param \Monolog\Logger $log
	 * @param \Doctrine\DBAL\Connection $db
	 * @param \Application\Application $app
	 */
	public function __construct(Array $project, Logger $log, Connection $db, Application $app) {
		
		$this->project = $project;
		$this->log = $log;
		$this->app = $app;
		$this->db = $db;
		
		$this->pid = getmypid();

		//Initialize project and navigator stettings
		$this->_initialize();
	}

	/**
	 * 
	 */
	private function _initialize() {
	    
		$this->navigator = new Navigator(
			$this->project, 
			$this->db->fetchAll('SELECT * FROM project_rule WHERE project=?', array($this->project['id'])),
			$this->log,
			$this->db
		);

		
		$processorClass = 'Scraper\Console\Parser\Processors\\' . $this->project['processor'] . 'Processor';
		$this->log->addNotice($this->pid . ' - init processing class', array($processorClass, class_exists($processorClass)));
		if(class_exists($processorClass)) {
			$this->processor = new $processorClass($this->app, $this->db, $this->log);
			$this->log->addDebug($this->pid . ' - processor initialized', array('processor' => $this->processor, 'class methods' => get_class_methods(get_class($this->processor))));
		}
	}

	/**
	 * 
	 * @param string $url
	 * @param type $info
	 * @return boolean|array $result[0] = headers from document, $result[1] = documents content
	 */
	private function _download($url, &$info) {

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_PROXY, 'switchproxy.proxify.net:7496');
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.16) Gecko/20120427 Firefox/15.0a1');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 7);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_FAILONERROR, false);
		curl_setopt($ch, CURLOPT_HEADER, true);
		
		$result = curl_exec($ch);
		$info = curl_getinfo($ch);
		if ($info['http_code'] != 200) {
			return false;
		}

		curl_close($ch);
		
		$result = explode("\r\n\r\n", "$result", 2); 
		
		sleep(3);

		return $result;
	}

	/**
	 * update thread with new url
	 * @param type $url
	 * @return boolean
	 */
	private function _updateThread($url) {
		$this->db->executeQuery("INSERT INTO `thread` 
			(`id`, `type`, `activity`, `update`, `project`, `action`) 
			VALUES 
			(?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `update`=?, `activity`=?", 
			array(
				$this->pid, 'download', $url, date("Y-m-d H:i:s"), $this->project['id'], Thread::ACTION_DOWNLOAD, date("Y-m-d H:i:s"), $url
			)
		);
		return true;
	}
	
	/**
	 * Check if result contains content from switchproxy
	 * @param type $document
	 * @param type $url_id
	 */
	protected function switchProxyCheck(&$document, $url_id) {
		if (stristr($document, 'Rate limit exceeded') !== FALSE) {
			$this->log->addNotice( $this->pid . " - Rate limit exceeded");
			$this->db->update('thread', array('status' => self::STATUS_RATE_LIMIT_EXCEEDED), array('id'=> $this->pid));
			sleep(60);
		} elseif (stristr($document, 'Malformed HTTP request') !== FALSE){
			$this->log->addNotice( $this->pid . " - Malformed URL (skipping...)");
			$this->db->update('urls', array('pid' => 0, 'priority' => 'n'), array('id'=> $url_id));
		} elseif (stristr($document, 'not a valid domain') !== FALSE){
			$this->log->addNotice( $this->pid . " - Malformed URL (skipping...)");
			$this->db->update('urls', array('pid' => 0, 'priority' => 'n'), array('id'=> $url_id));
		} elseif (stristr($document, 't find address for') !== FALSE){
			$this->log->addNotice( $this->pid . " - Malformed URL (skipping...)");
			$this->db->update('urls', array('pid' => 0, 'priority' => 'n', 'http_code' => 201), array('id'=> $url_id));
		}  else {
			$this->log->addNotice( $this->pid . json_encode($document));	
			sleep(60);
		}

		$this->log->addNotice($document);
	}
	
	/**
	 * 
	 * @param mixed $url_id
	 * @param string $url
	 * @param boolean $download
	 * @return boolean
	 */
	private function _processUrl($url_id, $url, $download = true) {
		$hash = "";
		$links = array();
		$prioritized = 0;
		
		//Download url and keep track of cUrl info
		$info = array(
			"http_code" => 102, 
			"size_download" => 0
		);		
		if ($download === true) {
			$result = $this->_download($url, $info);
			if(is_array($result) && count($result) > 1) {
				$document = $result[1];
			} else {
			$document = "";
			}
		} else {
			$document = "";
		}
		
		//Check if we didn't exceed any rate limits
		if (stristr($document, 'SwitchProxy') !== FALSE) {
			$this->switchProxyCheck($document, $url_id);
		} else {
			$this->db->update('thread', array('status' => self::STATUS_SCANNING), array('id'=> $this->pid));
			//Check if we received a valid document
			if ($document != "") {

				$hash = substr(md5($document), 0, 8);

				//Check if folder already exists
				$folder = __DIR__ . "/../../../../../app/files/scraper/download/" . $this->project['id'] . "/";
				if (!is_dir($folder)) {
					mkdir($folder, 0777);
				}
				
				
				try{
					//Write file to disk, parse and process it
					$filename = $folder . $url_id . ".html";
					if (file_put_contents($filename, $document) == true) {
						chmod($filename, 0777);

						$parser = new LinkParser($this->log, $this->db, $url, $document);
						$links = $parser->getLinks();
						$prioritized = $this->processLinks($url, $links);
						$this->log->addNotice( $this->pid . " - links processed");
					}
				} catch (\Exception $ex) {
					$this->log->addError( $this->pid . " - " . $ex->getMessage());
				}
			}

			//Update URL wih result
			$this->db->update('urls', array('pid' => 0, 'http_code' => $info['http_code'], 'size_download' => $info['size_download'], 'content_hash' => $hash, 'updated_at' => date("Y-m-d")), array('id' => $url_id));

			// Schedule Process
			$this->log->addAlert('CHECKING PROCESSOR', array(is_object($this->processor)));
			if(is_object($this->processor)) {
				$this->processor->scheduleProcess($this->project['id'], $url_id, $url);
			}
			
		}
		
		//Debug result
		if ($download == "") {
			$this->log->addNotice( $this->pid . " Skipped " . $url . "");
		} else {
			if($prioritized == false){
				$this->log->addNotice( $this->pid . " Downloaded " . $info['url'] . " " . (ceil($info['size_download'] / 1024) . "Kb (external links rejected by navigator)"));
			} else {
				$this->log->addNotice( $this->pid . " Downloaded " . $info['url'] . " " . (ceil($info['size_download'] / 1024) . "Kb (".count($links). " links, ".$prioritized." prioritized)"));
								
			}
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param type $url
	 * @param type $links
	 * @return int|boolean
	 */
	public function processLinks($url, $links) {

		$clsUri = new Uri();
		$this->log->addNotice($this->pid . " - found " . count($links) . " links");
		$source = $clsUri->explodeURL($url);
		$prioritized = 0;
		$subdomainRoots = array();

		//Only process links on valid website
		$processLinks = $this->navigator->processLinks($url);
		if ($processLinks === false) {
			return false;
		}
		
		$i = 0;
		//First process all internal links
		foreach ($links as $key => $value) {
			$i++;
			
			$target = $clsUri->explodeURL($key);
						
			//Check if the link is in same hostname
			if ($source['hostname'] == $target['hostname']) {
				$in_hostname = 'y';
			} else {
				$in_hostname = 'n';
			}

			//Check if the link is in same domain name
			if ($source['domain'] == $target['domain']) {
				$in_domain = 'y';
			} else {
				$in_domain = 'n';
			}

			//Initialize navigator and do navigation magic
//			$this->navigator->trackFolder($key);
			$priority = $this->navigator->prioritizeURL($key);

			//Builth URL object and check if it if we're going to skip it
			$url = array(
				'project_id' => $this->project['id'],
				'url' => $key,
				'in_hostname' => $in_hostname,
				'in_domain' => $in_domain,
				'priority' => $priority
			);

			//Skip this URL if it;s going to be skipped in the future
			if($this->navigator->followURL($url) === false){
				continue;
			}
			
			$this->log->addDebug( $this->pid . " - processedlink (" . $i . ") ", array($key, $value, 'in_domain' => $in_domain, 'in_hostname' => $in_hostname, 'priority' => $priority));

			if (strlen($key) > 0 && strlen($key) < 230) {

				$result = $this->db->executeQuery("
					INSERT INTO urls (project, url, incoming_links, priority, created_at)
					VALUES (?, ?, ?, ?, ?)
					ON DUPLICATE KEY UPDATE incoming_links=incoming_links+1
				", array($this->project['id'], $key, 1, $priority, date("Y-m-d"))
				);
			}
		}

		$this->log->addDebug( "Added ".  implode(", ", $subdomainRoots));
		
		return $prioritized;
	}
	
	/**
	 * 
	 */
	public function run() {
		$this->log->addDebug($this->pid . " - get project repository", array( 'app' => get_class($this->app)));

		$this->log->addDebug($this->pid . " - Start downloading urls");
		//while there are urls available for this batch.
		while ($url = $this->db->fetchAssoc("SELECT * FROM urls WHERE pid=? ORDER BY id ASC LIMIT 1", array($this->pid))) {
			
			$this->thread = $this->db->fetchAssoc("SELECT * FROM thread WHERE id=?", array($this->pid));
			
			if ($url !== false && count($url) != 0) {
				$this->log->addNotice($this->pid . " - downloading " . $url['url'], $url);
				//Detectie van duplicate content werkt niet
				$this->_updateThread($url['url']);
				
				//Downoad and process URL
				$download = $this->navigator->followURL($url);
				$this->log->addNotice($this->pid . " - followed " . $url['url'], $url);
				$run = $this->_processUrl($url['id'], $url['url'], $download);
				$this->log->addNotice($this->pid . " - processed " . $url['url'], $url);
			}
		}
	}
}
