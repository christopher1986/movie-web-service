<?php

namespace Scraper\Console\Scraper\Downloader;

use Models\Project;
use Monolog\Logger;
use Doctrine\DBAL\Connection;
use Scraper\Util\Uri;

/**
 * Description of Navigator
 *
 * @author Kevin
 */
class Navigator
{
	/**
	 * @var Logger 
	 */
	protected $log;
	
	/**
	 * @var Connection 
	 */
	protected $db;
	
	/**
	 * @var type 
	 */
	protected $uri;
	
	/**
	 * @var Project 
	 */
	protected $project;
	
	/**
	 * @var array 
	 */
	protected $rules = array();
	
	/**
	 *
	 * @var mixed pid 
	 */
	protected $pid; 

	/**
	 * Constructor
	 * @param \Entities\Project $project
	 * @param array $rules
	 * @param \Monolog\Logger $log
	 * @param \Commands\Scraper\Lib\Downloader\Conection $db
	 */
	public function __construct(Array $project, array $rules, Logger $log, Connection $db) {
		$this->project = $project;
		$this->rules = $rules;
		$this->log = $log;
		$this->db = $db;
		$this->uri = new Uri();
		$this->pid = getmypid();
	}

	private function _urlToFolders($url) {

		//Split protocol from URL
		$url = substr($url, strpos($url, "://") + 3);
		$hostname = $this->uri->getHostName($url);
		$folders = explode("/", $url);

		//Filter out hostname and root to get array with folders
		foreach ($folders as $key => $folder) {
			if ($folder == $hostname || $folder == "" || strpos($folder, ".") !== false || strpos($folder, "?") !== false || strpos($folder, "#") !== false) {
				unset($folders[$key]);
			}
		}

		return $folders;
	}

	public function processLinks($url) {

		//URL we want to process links from
		$target = $this->uri->explodeURL($url);

		//URL of the project
		$source = $this->uri->explodeURL($this->project['startingpoint']);

		//Check if we should follow links to a sub domain
		if ($target['domain'] == $source['domain']) {
			return true;
		}
		return false;
	}

	public function followURL($url) {

		//Explode URL into seperate parts
		$uri = $this->uri->explodeURL($url['url']);
		$folders = $this->_urlToFolders($url['url']);
		

		//Apply rules on URL
		$allgood = false;
		if ($this->rules !== false) {
			$contains = false;
			
			foreach ($this->rules as $rule) {
				if ($rule['priority'] > 0 && $this->matchRegex($url['url'], $rule['regex'])) {
					$contains = true;
				}
			}
						
			//URL may only be downloaded when all rules apply
			if ($contains == true) {
				return true;
			} else {
				$this->log->addDebug($this->pid . ' - ' . " skippedlink", array($url));
				return false;
			}
		}

		return true;
	}
	
	public function prioritizeURL($url) {
		//Explode URL into seperate parts
		$uri = $this->uri->explodeURL($url);
		
		//Apply rules on URL
		$priority = 0;
		if ($this->rules !== false) {
			foreach ($this->rules as $rule) {
				if ($rule['priority'] > 20 && $this->matchRegex($url, $rule['regex'])) {
					if($rule['priority'] > $priority) {
						$priority = $rule['priority'];
					}
				}
			}
		}
		return $priority;
	}
	
	/**
	 * 
	 * @param type $url
	 * @param type $content
	 * @return bool
	 */
	public function matchRegex($url, $content) {
		$pregMatch = preg_match($content, $url);
		if($pregMatch > 0) {
			return true;
		} elseif($pregMatch === 0) {
			return false;
		} else {
			$this->log->addDebug($this->pid . ' - Preg match error while checking regex rules', array($url));
		}
	}
	
}
