<?php

namespace Scraper\Console\Scraper;

use Framework\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Helper\QuestionHelper;
use Monolog\Logger;
use Doctrine\DBAL\Connection;

/**
 * Description of ScrapperCommand
 *
 * @author kevinpostma
 */
class ScraperCommand extends Command
{
    
    /**
     * @var \Framework\Application;
     */
    protected $app;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Connection
     */
    protected $connection;
    
    /**
     * Initiates the scraper
     * @param \Framework\Application $app
     * @param \Monolog\Logger $logger
     * @param type $name
     */
    public function __construct(Application $app, Logger $logger, $name = null) {
	parent::__construct($name);
	$this->app = $app;
	$this->logger = $logger;
	$this->connection = $app->getDatabaseConnection();
    }
    
    /**
     * Configure the console application
     */
    protected function configure()
    {
	$this
	    ->setName('scraper:download')
	    ->setDescription('Process scraped data.')
	    ->addArgument('method', \Symfony\Component\Console\Input\InputArgument::OPTIONAL, 'RUN|SCHEDULE|REFRESH', 'SCHEDULE')
	    ->addOption('projectid', 'projectid', \Symfony\Component\Console\Input\InputOption::VALUE_OPTIONAL, 'Project id', null);
    }

    /**
     * executes the console application
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
	$this->input = $input;
	$this->output = $output;

	// Check if we need to schedule
	$logger = $this->logger;
	$clsDownload = new Download($this->app, $this->connection, $this->logger);
	
	switch (strtoupper($input->getArgument('method'))) {
	    case "RUN":
		$this->logger->addInfo("START RUNNING NEW DOWNLOAD THREAD");
		try {
		    $projectid = $this->checkProjectId();
		    $clsDownload->run($projectid);
		} catch (\Exception $e) {
		    // cleaning up thread database if an error occured.
		    $this->connection->delete('thread', array('id' => getmypid()));
		    $this->connection->update('urls', array('pid' => 0), array('pid' => getmypid()));
		    $logger->addError($e->getMessage(), array($e->getTrace()));
		    $output->writeln($e->getMessage() . ' ' . json_encode(array($e->getTrace())));
		}
		break;
	    case "SCHEDULE":
		$clsDownload->schedule();
		break;
	    case "REFRESH":
		$projectid = $this->checkProjectId();
		$clsDownload->refresh($projectid);
		$this->output->writeln("PROJECT REFRESHED");
		$clsDownload->schedule();
		break;
	    default:

		break;
	}
    }

    /**
     * Checks if a projectid has been entered.
     * @return type
     */
    protected function checkProjectId() {
	
	// get the available projects
	$result = $this->connection->fetchAll('SELECT id, name FROM project');
	$availableProjects = array();
	foreach($result as $val) {
	    $availableProjects[] = $val['id'] . " - " . $val['name'];
	}
	
	// check if projectid is valid.
	$validProjectIds = function(array $ids) use ($result) {
	    $availableProjectIds = array_reduce($result, function($result = array(), $value){
		if(isset($value['id'])) $result[] = intval($value['id']);
		return $result;
	    });
	    
	    if(count($ids) == 0)
		return false;
	    
	    foreach($ids as $id) {
		if(!in_array($id, $availableProjectIds)) {
		    return false;
		}
	    }
	    
	    return true;
	};
	
	// check if a project id was given.
	// if is valid then return option project ids
	$optionProjectIds = (is_array($this->input->getOption('projectid')) ? $this->input->getOption('projectid') : array($this->input->getOption('projectid')));
	if($validProjectIds($optionProjectIds)) {
	    return $optionProjectIds;
	} else {
	    
	    // ask for projects
	    $question = new ChoiceQuestion(
		'Please enter the which projects that you want to have refreshed.',
		$availableProjects,
		0
	    );
	    
            //Setting an errormessage
	    $question->setErrorMessage('The projectid %s is invalid.');
	    $question->setMultiselect(true);
	    $helper = new QuestionHelper();
	    
	    
	    $projects = $helper->ask($this->input, $this->output, $question);
	    $ids = array();
	    foreach($projects as $project) {
		$projectid = explode("- ", $project);
		$projectid = intval(trim($projectid[0]));
		$ids[] = $projectid;
	    }
	    
	    if($validProjectIds($ids)) {
		return $ids;
	    } else {
		$this->checkProjectId();
	    }
	}
    }

}