<?php

namespace FooModule;

/**
 * A mock object that mimics a Module class.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class Module
{}
