<?php

namespace Test;

use Test\EventDispatcher\EventTest;

use Framework\EventDispatcher\EventDispatcher;
use Framework\EventDispatcher\Event;

class EventDispatcherTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the presence of event handler in EventDispatcher.
     */
    public function testEventHandlerPresence()
    {
        $handler = function(Event $event) {};
    
        $dispatcher = new EventDispatcher();
        $dispatcher->addEventHandler('click', $handler);
        
        
        $this->assertTrue(in_array($handler, $dispatcher->getEventHandlers('click'), true));
    }
    
    /**
     * Test the presence of event filter in EventDispatcher.
     */
    public function testEventFilterPresence()
    {
        $filter = function(Event $event) {};
    
        $dispatcher = new EventDispatcher();
        $dispatcher->addEventFilter('click', $filter);
        
        
        $this->assertTrue(in_array($filter, $dispatcher->getEventFilters('click'), true));
    }
    
    /**
     * Test that event handler is removed.
     */
    public function testRemoveEventHandler()
    {
        $handler = function(Event $event) {};
    
        $dispatcher = new EventDispatcher();
        $dispatcher->addEventHandler('click', $handler);
        $dispatcher->removeEventHandler('click', $handler);
        
        $this->assertFalse(in_array($handler, $dispatcher->getEventHandlers('click'), true));
    }
    
    /**
     * Test that event filter is removed.
     */
    public function testRemoveEventFilter()
    {
        $filter = function(Event $event) {};
    
        $dispatcher = new EventDispatcher();
        $dispatcher->addEventFilter('click', $filter);
        $dispatcher->removeEventFilter('click', $filter);
        
        $this->assertFalse(in_array($filter, $dispatcher->getEventFilters('click'), true));
    }
    
    /**
     * Test that an event can be consumed.
     */
    public function testFilterConsume()
    {    
        $event = new EventTest();
    
        $dispatcher = new EventDispatcher();
        $dispatcher->addEventHandler(EventTest::EVENT_NAME, function(EventTest $event) {
            $event->setValue('foobar');
        });       
        $dispatcher->addEventFilter(EventTest::EVENT_NAME, function(Event $event) {
            $event->consume();
        });
        $dispatcher->dispatch(EventTest::EVENT_NAME, $event);
        
        $this->assertNull($event->getValue());
    }
    
    /**
     * Test that an event can be dispatched.
     */
    public function testDispatch()
    {    
        $handler = function(EventTest $event) {
            $event->setValue('foobar');
        };
    
        $event = new EventTest();
    
        $dispatcher = new EventDispatcher();
        $dispatcher->addEventHandler(EventTest::EVENT_NAME, $handler);        
        $dispatcher->dispatch(EventTest::EVENT_NAME, $event);
        
        $this->assertEquals('foobar', $event->getValue());
    }
}
