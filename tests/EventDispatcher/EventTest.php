<?php

namespace Test\EventDispatcher;

use Framework\EventDispatcher\Event;

/**
 * A mock object that mimics an Event.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class EventTest extends Event
{
    /**
     * The name of the event.
     *
     * @var string
     */
    const EVENT_NAME = 'click';
    
    /**
     * A mixed value.
     *
     * @param mixed 
     */
    private $value;
    
    /**
     * Construct EventTest.
     */
    public function __construct()
    {}

    /**
     * Set mixed value.
     *
     * @param mixed $value the value to set.
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
    
    /**
     * Returns a value.
     *
     * @return mixed|null the value.
     */
    public function getValue()
    {
        return $this->value;
    }
}
