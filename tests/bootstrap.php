<?php

// setup directories.
$rootDir    = realpath(dirname(__DIR__));
$libraryDir = $rootDir . '/library';
$testDir    = $rootDir . '/tests';

// add directory to include path.
$path = array(
    $libraryDir,
    $testDir,
    get_include_path(),
);
set_include_path(implode(PATH_SEPARATOR, $path));

// include composer autoloader.
include_once $rootDir . '/vendor/autoload.php';
// include framework autoloader.
include $libraryDir . '/Framework/Loader/AutoloaderFactory.php';

// instantiate framework autoloader.
Framework\Loader\AutoloaderFactory::factory(array(
    'Framework\Loader\StandardAutoloader' => array(
        'autoregister_framework' => true,
        'namespaces' => array(
            'Test' => $testDir,
        ),
    ),
));

// unset variables
unset($rootDir, $libraryDir, $testDir, $path);
    
