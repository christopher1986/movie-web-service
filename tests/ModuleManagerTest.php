<?php

namespace Test;

use Framework\Config\Config;
use Framework\ModuleManager\ModuleManager;
use Framework\ModuleManager\ModuleConfig;
use Framework\ModuleManager\Event\ModuleEvent;

class ModuleManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test that module is loaded.
     */
    public function testModuleLoaded()
    {
        $config = new Config(array(
            'paths' => array(
                'module' => __DIR__ .'/src/',
            ),
        ));
        
        $moduleConfig = new ModuleConfig('FooModule', true);
        
        $moduleManager = new ModuleManager($config);
        $moduleManager->loadModule($moduleConfig);
        
        $this->assertTrue($moduleManager->getLoadedModules()->contains($moduleConfig));
    }
    
    /**
     * Test that inactive modules are not loaded.
     */
    public function testInactiveModuleLoading()
    {
        $config = new Config(array(
            'paths' => array(
                'module' => __DIR__ .'/src/',
            ),
        ));
        
        $moduleConfig = new ModuleConfig('BarModule', false);
        
        $moduleManager = new ModuleManager($config);
        $moduleManager->getLoadedModules();
        
        $this->assertFalse($moduleManager->getLoadedModules()->contains($moduleConfig));
    }
}
