<?php

namespace Test;

use Slim\Slim;

use Framework\Slim\Middleware\ResponseMiddleware;
use Framework\Slim\Http\JsonResponse;
use Framework\Slim\Http\Response;

class SlimTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test ResponseMiddleware setting HTTP Status-Code.
     */
    public function testStatusCode()
    {
        \Slim\Environment::mock(array(
            'REQUEST_METHOD' => 'GET',
            'SCRIPT_NAME' => '/foo', //<-- Physical
            'PATH_INFO' => '/bar', //<-- Virtual
        ));
    
        $slim = new Slim();
        $slim->add(new \Framework\Slim\Middleware\ResponseMiddleware());
        $slim->get('/bar', function () {
            return new JsonResponse(array('foo' => 'bar'), Response::HTTP_UNAUTHORIZED);
        });
        $slim->run();
        
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $slim->response()->getStatus());
    }
    
    /**
     * Test ResponseMiddleware setting Content-Type header.
     */
    public function testContentType()
    {
        \Slim\Environment::mock(array(
            'REQUEST_METHOD' => 'GET',
            'SCRIPT_NAME' => '/foo', //<-- Physical
            'PATH_INFO' => '/bar', //<-- Virtual
        ));
    
        $slim = new Slim();
        $slim->add(new \Framework\Slim\Middleware\ResponseMiddleware());
        $slim->get('/bar', function () {
            return new Response('test', Response::HTTP_OK, 'application/xml');
        });
        $slim->run();
        
        $this->assertEquals('application/xml', $slim->response()->header('Content-Type'));
    }
}
