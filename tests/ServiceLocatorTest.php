<?php

namespace Test;

use Framework\ServiceLocator\ServiceLocator;
use Framework\EventDispatcher\EventDispatcher;

class ServiceLocatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test creation of invokable service.
     */
    public function testServiceCreation()
    {
        $locator = new ServiceLocator();
        $locator->invokable('dispatcher', 'Framework\EventDispatcher\EventDispatcher');
        
        $this->assertInstanceOf('Framework\EventDispatcher\EventDispatcher', $locator->get('dispatcher'));
    }
    
    /**
     * Test that services are shared.
     */
    public function testSharedService()
    {
        $locator = new ServiceLocator();
        $locator->factory('dispatcher', function($locator) {
            return new EventDispatcher();
        });
        
        $firstDispatcher  = $locator->get('dispatcher');
        $secondDispatcher = $locator->get('dispatcher');
        
        $this->assertSame($firstDispatcher, $secondDispatcher);
    }

    /**
     * Test that sharing can be disabled.
     */
    public function testNotSharedService()
    {
        $locator = new ServiceLocator();
        $locator->factory('dispatcher', function($locator) {
            return new EventDispatcher();
        });
        $locator->shared('dispatcher', false);
        
        $firstDispatcher  = $locator->get('dispatcher');
        $secondDispatcher = $locator->get('dispatcher');
        
        $this->assertNotSame($firstDispatcher, $secondDispatcher);
    }
}
