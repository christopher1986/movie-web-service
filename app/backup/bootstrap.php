<?php

/*@var $slim \Slim\Slim */
$slim;

require_once __DIR__ . '/config/config.php';

/**
 * Initialize database
 */
$scraperDatabaseConfig = new \Doctrine\DBAL\Configuration();
$scraperDatabaseConnectionparams = array(
    'dbname'	=> $slim->config('database.scraper.db'),
    'user'	=> $slim->config('database.scraper.user'),
    'password'	=> $slim->config('database.scraper.pass'),
    'host'	=> $slim->config('database.scraper.host'),
    'driver'	=> $slim->config('database.scraper.driver'),
);
$slim->container->singleton('doctrine.database.scraper', \Doctrine\DBAL\DriverManager::getConnection($scraperDatabaseConnectionparams, $scraperDatabaseConfig));


/**
 * Initialize doctrine mongodb mapper
 */
//use Doctrine\MongoDB\Connection;
//use Doctrine\ODM\MongoDB\Configuration;
//use Doctrine\ODM\MongoDB\DocumentManager;
//use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
//
//AnnotationDriver::registerAnnotationClasses();
//
//$config = new Configuration();
//$config->setProxyDir(__DIR__ . '/');
//$config->setProxyNamespace('Proxies');
//$config->setHydratorDir('/path/to/generate/hydrators');
//$config->setHydratorNamespace('Hydrators');
//$config->setMetadataDriverImpl(AnnotationDriver::create('/path/to/document/classes'));
//
//$dm = DocumentManager::create(new Connection(), $config);



return $slim;
