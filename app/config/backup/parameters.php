<?php

/*@var $slim \Slim\Slim */
$slim;

/**
 * Set database connection params
 */
$slim->config('database.scraper.host'	, 'localhost');
$slim->config('database.scraper.user'	, 'root');
$slim->config('database.scraper.pass'	, '');
$slim->config('database.scraper.db'	, 'scraper');
$slim->config('database.scraper.driver'	, 'pdo_mysql');

/**
 * Set mongodb connection params
 */
$slim->config('mongodb.scraper.imdb.host'	, 'vps2.kpict.nl');
$slim->config('mongodb.scraper.imdb.db'		, 'movie_webservice_imdb');

