<?php

return array(
    'module' => array(
        array(
            'name'     => 'Movies',
            'active'   => true,
            'priority' => 20,
        ),
        array(
            'name'     => 'Scraper',
            'active'   => true,
            'priority' => 10,
        ),
    )
);
