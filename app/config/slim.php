<?php

return array(
    'debug'          => true,
    'log.enabled'    => false,
    'log.level'      => \Slim\Log::DEBUG,
    'templates.path' => './app/templates/'
);
