<?php 

return array(
    'config' => array(
        'modules' => './app/config/modules.php',
        'slim'    => './app/config/slim.php',
    ),
    'paths' => array(
        'module' => './src/',
    ),
    'databases' => array(
        'mongodb' => array(
            'host'    => 'vps2.kpict.nl',
            'dbname' => 'movie_webservice_imdb',
        ),
        'mysql' => array(
            'host'    => '127.0.0.1',
            'user'    => 'root',
            'pass'    => '',
            'dbname' => 'scraper_test_sc',
            'driver'  => 'pdo_mysql',
        ),
    ),
);
