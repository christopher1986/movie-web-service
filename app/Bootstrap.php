<?php

use Framework\Application;
use Framework\ModuleManager\Event\ModuleEvent;
use Framework\DBAL\DoctrineAware;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration as MongodbConfiguration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;


require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/library/Framework/Application.php';

class Bootstrap extends Application
{    
    /**
     * {@inheritDoc}
     */
    public function registerConfiguration()
    {
        return sprintf('%s/config/config.php', __DIR__);
    }
    
    /**
     * Register services with the service manager.
     *
     * Services can be directly registered with the manager or be created by the manager through
     * the use of a fully qualified class name (invokable) or a factory whose sole task is to
     * create and return the service.
     *
     * @return void
     */
        private function _initServices()
        {
            $locator = $this->getServiceLocator();
            $locator->factory('MySQLConnection', function($locator) {
                $dbalParams = $locator->get('Config')->get('databases')->get('mysql')->toArray();        
                return DriverManager::getConnection($dbalParams, new Configuration());
            });
            $locator->factory('DocumentManager', function($locator) {
                $config = $locator->get('Config');
                $host = $config->get('databases')->get('mongodb')->get('host');

                AnnotationDriver::registerAnnotationClasses();

                $mongodbConfig = new MongodbConfiguration();
                $mongodbConfig->setProxyNamespace('Movies\Models\Mongodb\Proxies');
                $mongodbConfig->setHydratorNamespace('Movies\Models\Mongodb\Hydrators');
                $mongodbConfig->setProxyDir(dirname(__DIR__) . '/src/Movies/Models/Mongodb/Proxies');
                $mongodbConfig->setHydratorDir(dirname(__DIR__) . '/src/Movies/Models/Mongodb/Hydrators');
                $mongodbConfig->setMetadataDriverImpl(AnnotationDriver::create(__DIR__ . '../src/Movies/Models/Mongodb'));

                return DocumentManager::create(new Connection($host), $mongodbConfig);
            });
        }
    
    /**
     * Returns a Doctrine DBAL connection.
     *
     * @return Doctrine\DBAL\Connection a Doctrine DBAL connection.
     */
    public function getDatabaseConnection() 
    {
	    return $this->getServiceLocator()->get('MySQLConnection');
    }
    
    /**
     * @return DocumentManager
     */
    public function getDocumentManager() 
    {
	    return $this->getServiceLocator()->get('DocumentManager');
    }
}
