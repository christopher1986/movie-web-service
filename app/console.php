<?php

// change directory to root.
chdir(dirname(__DIR__));

require_once 'app/Bootstrap.php';

$bootstrap = new Bootstrap();

use Symfony\Component\Console\Application;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * setting the logger for the scraper
 */
$scrapeLogger = new Logger('scraper');
$scrapeLogger->pushHandler(new StreamHandler(__DIR__.'/files/scraper/logs/download.log', Logger::DEBUG));

$processLogger = new Logger('scraper');
$processLogger->pushHandler(new StreamHandler(__DIR__.'/files/scraper/logs/process.log', Logger::DEBUG));

$application = new Application('Movie webservice', '0.1');
$application->add(new Scraper\Console\Scraper\ScraperCommand($bootstrap, $scrapeLogger));
$application->add(new Scraper\Console\Parser\ProcessCommand($bootstrap, $processLogger));
$application->run();
