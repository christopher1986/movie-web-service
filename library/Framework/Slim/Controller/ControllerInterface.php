<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Controller;

use Framework\ServiceLocator\ServiceAwareInterface;
use Framework\Slim\Router\RouteAwareInterface;

/**
 * The ControllerInterface describes the methods a controller should implements.
 * 
 * @author Chris Harris 
 * @version 0.0.1
 */
interface ControllerInterface extends RouteAwareInterface, ServiceAwareInterface
{    
    /**
     * Returns true if a parameter exists for the given name.
     *
     * @param string $name the parameter name to be tested.
     * @return bool true if a parameter exists for the given name, false otherwise.
     */
    public function hasParam($name);
    
    /**
     * Set a collection of parameters.
     *
     * An associative array is returned by this method containing all the
     * previously stored values for parameters that already exists.
     *
     * @param array|Traversable a collection of parameters.
     * @return array a collection of previously stored parameter values.     
     * @see AbstractController::setParam($name, $value)
     */
    public function setParams($params);
    
    /**
     * Set a new parameter for this controller.
     *
     * If a parameter already existed for the given name it will be overriden
     * and the previous value value by the parameter is returned.
     *
     * @param string $name the name of the parameter.
     * @param mixed $value the value to store under the parameter.
     * @return mixed|null the previous value, or null if the parameter did not exist.
     */
    public function setParam($name, $value);
    
    /**
     * Returns the value for the given parameter, or the default value if the parameter does not exist.
     *
     * @param string $name the name of the parameter.
     * @param mixed $default the value to return if the parameter does not exist.
     * @return mixed the value for the given parameter, or the default value.
     */
    public function getParam($name, $default = null);
    
    /**
     * Returns all parameters stored by this controller.
     *
     * @return array a collection of parameters.
     */
    public function getParams();
}
