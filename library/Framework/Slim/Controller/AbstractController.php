<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Controller;

use Framework\ServiceLocator\ServiceAwareInterface;
use Framework\ServiceLocator\ServiceLocatorInterface;
use Slim\Route;

/**
 * A base class from which all controllers are derived.
 *
 * The AbstractController grants access to information stored by the
 * route. A Route object contains information about the request made 
 * and contains information that is sometimes needed by a controller.
 *
 * @author Chris Harris
 * @version 0.0.1
 */
abstract class AbstractController implements ControllerInterface
{
    /**
     * The route for which this controller instantiated.
     * 
     * @var Route
     */
    private $route;

    /**
     * A locator that contains and creates services.
     *
     * @var ServiceLocatorInterface
     */
    private $locator;

    /**
     * Slim HTTP Request.
     *
     * @var Request;
     */
    private $request;

    /**
     * A collection of parameters.
     *
     * @var array
     */
    private $params = array();

    /**
     * {@inheritDoc}
     */
    public function setServiceLocator(ServiceLocatorInterface $locator)
    {
        $this->locator = $locator;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getServiceLocator()
    {
        return $this->locator;
    }
    
    /**
     * Returns the Request object.
     *
     * @return Request a Slim HTTP Request.
     * @see Slim\Slim::request()
     */
    public function getRequest()
    {
        if ($this->request === null) {
            $this->request = $this->getServiceLocator()->get('Slim')->request();
        }
        return $this->request;
    }

    /**
     * {@inheritDoc}
     *
     * The parameters contained by the route are added to the parameters
     * of this controller. Any previously stored value under an existing
     * parameter will be overriden by the parameters of the route.
     *
     * @param Route $route the route that matched the request uri.
     */
    public function setRoute(Route $route)
    {
        $params = $route->getParams();
        if (is_array($params)) {
            $this->params = array_merge($this->params, $params);
        }
        
        $this->route = $route;
    }
    
    /**
     * {@inheritDoc}
     */
    public function hasParam($name)
    {
        return (isset($this->params[$name]));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setParams($params)
    {
        if (!is_array($params) && !($params instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s expects an array or Traversable object as argument; received "%d"',
                __METHOD__,
                (is_object($params) ? get_class($params) : gettype($params))
            ));
        }
        
        $oldValues = array();
        foreach ($params as $name => $value) {
            if (($oldValue = $this->setParam($name, $value)) !== null) {
                $oldValues[$name] = $oldValue;
            }
        }
        
        return $oldValues;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setParam($name, $value)
    {
	    if (!is_string($name)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($name) ? get_class($name) : gettype($name))
            ));
	    }
    
        $oldValue = null;
        if ($this->hasParam($name)) {
            $oldValue = $this->getParam($name);
        }
    
        $this->params[$name] = $value;
        
        return $oldValue;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getParam($name, $default = null)
    {
        return ($this->hasParam($name)) ? $this->params[$name] : $default;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getParams()
    {
        return $this->params;
    }
}
