<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Common;

use ReflectionClass;

/**
 * This class represents a callable function. The Callable class reports information
 * about a callable.
 *
 * @author Chris Harris
 * @version 0.0.1
 */
class CallableDescriptor
{
    /**
     * The underlying obect of this callable.
     *
     * @var object
     */
    private $object;

    /**
     * The callable to describe.
     *
     * @var callable
     */
    private $callable;
    
    /**
     * Construct a callable.
     *
     * @param callable $callable the callable this object describes.
     */
    public function __construct($callable)
    {
        $this->setCallable($callable);
    }
    
    /**
     * Returns true if this callable will invoke a function.
     *
     * @return bool true if the callable invokes a function, false otherwise.
     */
    public function isFunction()
    {
        return (is_string($this->callable));
    }

    /**
     * Returns true if this callable will invoke a method.
     *
     * @return bool true if the callable invokes a method, false otherwise.
     */
    public function isMethod()
    {
        return (is_array($this->callable) && count($this->callable) == 2);
    }
    
    /**
     * Returns true if this callable will invoke a static method.
     *
     * @return bool true if the callable invokes a static method, false otherwise.
     */
    public function isStaticMethod()
    {
        if ($this->isMethod()) {
            return (is_string($this->callable[0]));
        }
        return false;
    }
    
    /**
     * Set the callable that this object describes.
     *
     * @param callable $callable a callable this object describes.
     * @throws InvalidArgumentException if the given argument is not a callable.
     */
    private function setCallable($callable)
    {
        if (!is_callable($callable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: the given argument cannot be called as a function; received "%s"',
                __METHOD__,
                (is_object($callable) ? get_class($callable) : gettype($callable))
            ));
        }
       
        $this->callable = $callable;
    }
    
    /**
     * Returns the callable that this object describes.
     *
     * @return callable the callable this object describes.
     */
    public function getCallable()
    {
        return $this->callable;
    }
    
    /**
     * Returns if present the class name of the underlying object of the callable.
     *
     * If the callable represents a function this method will return a NULL literal,
     * otherwise the name of the class is returned.
     *
     * @return string|null the name of the class, or null if the callable is a function.
     */
    public function getClassName()
    {
        $className = null;
        if ($this->isStaticMethod()) {
            $className = $this->callable[0];
        } else if ($this->isMethod()) {
            $className = get_class($this->callable[0]);
        }
        
        return $className;
    }
    
    /**
     * Returns if present the underying object of the callable.
     *
     * If the callable represenst a function this method will return a NULL literal,
     * otherwise if the callable represents a static method a new instance of the class
     * will be instantiated.
     *
     * @return object|null the underlying object of the callable, or null if the callable is a function.
     */
    public function getObject()
    {        
        if ($this->object !== null) {
            return $this->object;
        }
        
        if ($this->isStaticMethod()) {
            $ref = new ReflectionClass($this->getClassName());
            if ($ref->isisInstantiable() && $ref->getConstructor()->getNumberOfRequiredParameters() == 0) {
                $this->object = $ref->newInstance();
            }
        } else if ($this->isMethod()) {
            $this->object = $this->callable[0];
        }
        
        return $this->object;
    }
}
