<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Middleware;

use Framework\Slim\Http\ResponseInterface;

use \Slim\Middleware;

/**
 * The ResponseMiddleware ensures that the content of a Response object is displayed
 * to the client.
 *
 * @author Chris Harris
 * @version 0.0.1
 */
class ResponseMiddleware extends Middleware
{
    /**
     * Register a hook that is dispatched when the router has matched a request url to a route.
     *
     * @see Slim::call()
     * @see self::onRoutesMatched()
     */
    public function call()
    {
        // apply listener to slim application.
        $this->app->hook('slim.after.router', array($this, 'onRoutesMatched'), 1);
        // call next middleware.
        $this->next->call();
    }
    
    /**
     * Called after the router has found zero or more matching routes.
     *
     * Slim uses output buffering to capture the ouput of a callback function. A controller however is allowed to return a  
     * {@see ResponseInterface} object. This middleware calls the same callback function as Slim did and determines whether
     * it returns any value. So to prevent duplicate content from being displayed we simply clear the output buffer before
     * making a second call to the callback function.
     *
     * @see self::render($response)
     * @see Slim\Route::dispatch()
     */
    public function onRoutesMatched()
    {
        // erase content of previous calls.
        if (ob_get_level()) {
            ob_clean();
        }
    
        $routes = $this->app->router->getMatchedRoutes($this->app->request->getMethod(), $this->app->request->getResourceUri());

        $retval = null;
        do {
            // get and remove first route.
            $route = array_shift($routes);
            
            // call middleware associated with route.
            foreach ($route->getMiddleware() as $mw) {
                call_user_func_array($mw, array($route));
            }
            
            // get return value of callback function.
            $retval = call_user_func_array($route->getCallable(), array_values($route->getParams()));
            if ($retval instanceof ResponseInterface) {
                $this->render($retval);
            }
        } while (!$retval && !empty($routes));   
    }
    
    /**
     * Render the given response object.
     *
     * @param ResponseInterface the response object to render.
     * @see Slim::call()
     */
    private function render(ResponseInterface $response)
    {
        $this->app->status($response->getStatusCode());
        $this->app->contentType($response->getContentType());
        
        echo $response->getContent();
    }
}
