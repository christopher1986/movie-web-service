<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Http;

/**
 * A basic response that holds the information that will be displayed to the client.
 *
 * Besides the actual content to display a response object also contains the response
 * code and content type.
 *
 * @autor Chris Harris
 * @version 0.0.1
 */
class Response implements ResponseInterface
{
    /**
     * HTTP status code 200: OK.
     *
     * @var int
     */
    const HTTP_OK = 200;

    /**
     * HTTP status code 201: Created.
     *
     * @var int
     */
    const HTTP_CREATED = 201;

    /**
     * HTTP status code 202: Accepted.
     *
     * @var int
     */
    const HTTP_ACCEPTED = 202;

    /**
     * HTTP status code 203: Non-Authoritative Information.
     *
     * @var int
     */
    const HTTP_NOT_AUTHORITATIVE = 203;

    /**
     * HTTP status code 204: No Content.
     *
     * @var int
     */
    const HTTP_NO_CONTENT = 204;

    /**
     * HTTP status code 205: Reset Content.
     */
    const HTTP_RESET = 205;

    /**
     * HTTP status code 206: Partial Content.
     *
     * @var int
     */
    const HTTP_PARTIAL = 206;

    /**
     * HTTP status code 300: Multiple Choices.
     *
     * @var int
     */
    const HTTP_MULT_CHOICE = 300;

    /**
     * HTTP status code 301: Moved Permanently.
     *
     * @var int
     */
    const HTTP_MOVED_PERM = 301;

    /**
     * HTTP status code 302: Temporary Redirect.
     *
     * @var int
     */
    const HTTP_MOVED_TEMP = 302;

    /**
     * HTTP status code 303: See Other.
     *
     * @var int
     */
    const HTTP_SEE_OTHER = 303;

    /**
     * HTTP status code 304: Not Modified.
     *
     * @var int
     */
    const HTTP_NOT_MODIFIED = 304;

    /**
     * HTTP status code 305: Use Proxy.
     *
     * @var int
     */
    const HTTP_USE_PROXY = 305;
    
    /**
     * HTTP status code 400: Bad Request.
     *
     * @var int
     */
    const HTTP_BAD_REQUEST = 400;

    /**
     * HTTP status code 401: Unauthorized.
     *
     * @var int
     */
    const HTTP_UNAUTHORIZED = 401;

    /**
     * HTTP status code 402: Payment Required.
     *
     * @var int
     */
    const HTTP_PAYMENT_REQUIRED = 402;

    /**
     * HTTP status code 403: Forbidden.
     *
     * @var int
     */
    const HTTP_FORBIDDEN = 403;

    /**
     * HTTP status code 404: Not Found.
     *
     * @var int
     */
    const HTTP_NOT_FOUND = 404;

    /**
     * HTTP status code 405: Method Not Allowed.
     *
     * @var int
     */
    const HTTP_BAD_METHOD = 405;

    /**
     * HTTP status code 406: Not Acceptable.
     *
     * @var int
     */
    const HTTP_NOT_ACCEPTABLE = 406;

    /**
     * HTTP status code 407: Proxy Authentication Required.
     *
     * @var int
     */
    const HTTP_PROXY_AUTH = 407;

    /**
     * HTTP status code 408: Request Time-Out.
     *
     * @var int
     */
    const HTTP_CLIENT_TIMEOUT = 408;

    /**
     * HTTP status code 409: Conflict.
     *
     * @var int
     */
    const HTTP_CONFLICT = 409;

    /**
     * HTTP status code 410: Gone.
     *
     * @var int
     */
    const HTTP_GONE = 410;

    /**
     * HTTP status code 411: Length Required.
     *
     * @var int
     */
    const HTTP_LENGTH_REQUIRED = 411;

    /**
     * HTTP status code 412: Precondition Failed.
     *
     * @var int
     */
    const HTTP_PRECON_FAILED = 412;

    /**
     * HTTP status code 413: Request Entity Too Large.
     *
     * @var int
     */
    const HTTP_ENTITY_TOO_LARGE = 413;

    /**
     * HTTP status code 414: Request-URI Too Large.
     *
     * @var int
     */
    const HTTP_REQ_TOO_LONG = 414;

    /**
     * HTTP status code 415: Unsupported Media Type.
     *
     * @var int
     */
    const HTTP_UNSUPPORTED_TYPE = 415;

    /**
     * HTTP status code 500: Internal Server Error.
     *
     * @var int
     */
    const HTTP_INTERNAL_ERROR = 500;

    /**
     * HTTP status code 501: Not Implemented.
     *
     * @var int
     */
    const HTTP_NOT_IMPLEMENTED = 501;

    /**
     * HTTP status code 502: Bad Gateway.
     *
     * @var int
     */
    const HTTP_BAD_GATEWAY = 502;

    /**
     * HTTP status code 503: Service Unavailable.
     *
     * @var int
     */
    const HTTP_UNAVAILABLE = 503;

    /**
     * HTTP status code 504: Gateway Timeout.
     *
     * @var int
     */
    const HTTP_GATEWAY_TIMEOUT = 504;

    /**
     * HTTP status code 505: HTTP Version Not Supported.
     *
     * @var int
     */
    const HTTP_VERSION = 505;
    
    /**
     * The underlying data to display.
     *
     * @var mixed
     */
    protected $content;
    
    /**
     * The HTTP status code.
     *
     * @var int
     */
    protected $status;
    
    /**
     * A content type for this response.
     *
     * @var string
     */
    protected $contentType;
    
    /**
     * Creates a Response.
     *
     * @param mixed $content the content to display.
     * @param int $status the status code of this response.
     * @param string $contentType the content type of this response.
     */
    public function __construct($content = '', $status = self::HTTP_OK, $contentType = 'text/plain')
    {
        $this->setContent($content);
        $this->setStatusCode($status);
        $this->setContentType($contentType);
    }
    
    /**
     * {@inheritDoc}
     */
    public function getContent()
    {
        return $this->getRawContent();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getRawContent()
    {
        return $this->content;
    }
    
    /**
     * Set the content of this response.
     *
     * @return string the content.
     * @see Response::getRawContent()
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getStatusCode()
    {
        return $this->status;
    }
    
    /**
     * Set the HTTP status code for this response.
     *
     * @return int the HTTP status code.
     * @throws InvalidArgumentException if the given argument is not a number.
     */
    public function setStatusCode($status)
    {
        if (!is_numeric($status)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($status) ? get_class($status) : gettype($status))
            ));
        }
    
        $this->status = (int) $status;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getContentType()
    {
        return $this->contentType;
    }
    
    /**
     * Set the content type for this response.
     *
     * @return string the content type.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     */
    public function setContentType($contentType)
    {
        if (!is_string($contentType)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($contentType) ? get_class($contentType) : gettype($contentType))
            ));
        }
        
        $this->contentType = $contentType;
    }
}
