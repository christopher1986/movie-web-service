<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Http;

use Slim\Slim;

/**
 * A TemplateResponse displays information using a template. By default the content type is 
 * set to 'text/html'. It's a convenient object that one could use if the content is used for 
 * representation purposes or too large to be contained within a single string.
 *
 * @author Chris Harris
 * @version 0.0.1
 */
class TemplateResponse extends Response
{
    /**
     * The Slim application.
     *
     * @var Slim|null
     */
    private $app;

    /**
     * The path to a template to display by the view.
     *
     * @var string.
     */
    private $template = '';
    
    /**
     * An associative array of data made available to the view.
     *
     * @var array
     */
    private $data = array();
    
    /**
     * Create TemplateResponse.
     *
     * @param string $template the template to display by the view.
     * @param array|Traversable $data the data to be made available to the view.
     * @param int $status the status code of this response.
     * @param string $contentType the content type of this response.
     */
    public function __construct($template, $data = array(), $status = Response::HTTP_OK, $contentType = 'text/html')
    {
        $this->setTemplate($template);
        $this->setData($data);
        
        parent::__construct('', $status, $contentType);
    }
    
    /**
     * {@inheritDoc}
     */
    public function getContent()
    {
        $app = $this->getApp();
        if ($app === null) {
            throw new \LogicException(sprintf(
                '%s: unable to render template; make sure a slim application has been initialized.',
                __METHOD__
            ));
        }
        
        $app->view->appendData($this->getData());
        return $app->view->fetch($this->getTemplate());
    }
    
    /**
     * {@inheritDoc}
     */
    public function getRawContent()
    {
        return $this->getData();
    }
    
    /**
     * Set the template to display by the view.
     *
     * @param string $template the template to display.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     */
    public function setTemplate($template)
    {
        if (!is_string($template)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($template) ? get_class($template) : gettype($template))
            ));
        }
    
        $this->template = $template;
    }
    
    /**
     * Returns the template to display by the view.
     *
     * @return string the template to display.
     */
    public function getTemplate()
    {
        return $this->template;
    }
    
    /**
     * Set the data that will be made available to the view.
     *
     * @param array|Traversable $data the data to be made available to the view.
     * @throws InvalidArgumentException if the given argument is not an array or instance of Traversable.
     */
    public function setData($data)
    {
        if (!is_array($data) && !($data instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s expects an array or Traversable object as argument; received "%d"',
                __METHOD__,
                (is_object($data) ? get_class($data) : gettype($data))
            ));
        }
        
        if ($data instanceof \Traversable) {
            $data = iterator_to_array($data);
        }
        
        $this->data = $data;
    }
    
    /**
     * Returns the data will be made available to the view.
     *
     * @return array the data to be made available to the view.
     */
    public function getData()
    {
        return $this->data;
    }
    
    /**
     * Set the Slim application.
     *
     * The Slim application holds a view that is capable of rendering the template. 
     * See {@link http://docs.slimframework.com/#View-Overview View Overview} for more
     * information on the subject.
     *
     * @param Slim $app the Slim application.
     * @throws InvalidArgumentException if the given argument is a NULL literal.
     */
    public function setApp(Slim $app)
    {
        if ($app === null) {
            throw new \InvalidArgumentException(sprintf(
                '%s: does not allow a NULL literal',
                __METHOD__
            ));
        }
        
        $this->app = $app;
    }
    
    /**
     * Returns the Slim application.
     *
     * The Slim application provides a view that is capable of rendering the template.
     * See {@link http://docs.slimframework.com/#Application-Names-and-Scopes Application Names and Scopes} for more
     * information on how the Slim application is retrieved if one is not provided to the TemplateResponse.
     *
     * @return Slim|null the Slim application, or 'null' if a slim application has not yet been instantiated.
     */
    private function getApp()
    {
        if ($this->app === null) {
            $this->app = Slim::getInstance();
        }
        
        return $this->app;
    }
}
