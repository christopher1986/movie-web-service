<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Http;

/**
 * The ResponseInterface describes a container object that not only holds the data to
 * display to the client, but also contains the status code and content type of the response.
 *
 * @author Chris Harris
 * @version 0.0.1
 */
interface ResponseInterface
{
    /**
     * Returns the content of this response.
     *
     * A string representation of the actual content is returned by this
     * method; use the {@link ResponseInterface::getRawContent()} method
     * if you need the underlying data.
     *
     * @return string the content.
     */
    public function getContent();
    
    /**
     * Returns the underlying data of this response.
     *
     * This method should not be used to print information to the browser's
     * screen. Instead use the {@link ResponseInterface::getContent()} method
     * to display a string representation of the data.
     *
     * @return mixed the underlying data.
     */
    public function getRawContent();
    
    /**
     * Returns the HTTP status code for this response.
     *
     * @return int the HTTP status code.
     */
    public function getStatusCode(); 
    
    /**
     * Returns the content type for this response.
     *
     * @return string the content type.
     */
    public function getContentType();
}
