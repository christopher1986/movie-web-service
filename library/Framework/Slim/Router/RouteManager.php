<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Router;

use SplObjectStorage;
use Framework\EventDispatcher\EventDispatcher;
use Framework\EventDispatcher\EventDispatcherInterface;
use Framework\EventDispatcher\EventDispatcherAwareInterface;
use Framework\Slim\Common\CallableDescriptor;
use Framework\Slim\Router\Event\RouteEvent;
use Framework\Util\Arrays;
use Slim\Slim;
use Slim\Route;

class RouteManager implements EventDispatcherAwareInterface
{
    /**
     * The Slim application.
     *
     * @var Slim|null
     */
    private $app;
    
    /**
     * An object to notify event listeners.
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    
    /**
     * A collection of routes.
     *
     * @var SplObjectStorage
     */
    private $routes;
    
    /**
     * Construct a RouteManager.
     *
     * @param Slim $app the Slim application.
     */
    public function __construct(Slim $app)
    {
        $this->setApp($app);
    }

    /**
     * Create routes from data.
     *
     * @param array|Traversable $options the data from which to create the routes.
     * @throws InvalidArgumentException if the given argument is not an array or Traversable object.
     */
    public function factory($options)
    {
        if (!is_array($options) && !($options instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s expects an array or Traversable object as argument; received "%d"',
                __METHOD__,
                (is_object($options) ? get_class($options) : gettype($options))
            ));
        }
        
        foreach ($options as $option) {          
            if (isset($option['route'])) {
                $data = (isset($option['defaults'])) ? $option['defaults'] : null;
                $metadata = $this->createMetadata($option['route'], $data);
                
                if (($route = $this->createRoute($metadata['pattern'], $metadata['callable'], $metadata['method'])) !== null) {
                    // associate metadata with route.
                    $this->getRoutes()->attach($route, $metadata);
                }
            }
        }  
    }
    
    /**
     * Returns an associative array of metadata for the given route.
     *
     * Although the metadata returned by this method can consist of many key-value pairs 
     * it minimum structure will look as the following:
     *
     * array(
     *     'pattern'  => '',
     *     'method'   => 'get',
     *     'callable' => array(object(Slim\Slim)#81, 'defaultNotFound')
     * )
     *
     * @param array $route data that is related to a route.
     * @param array|null (optional) additional data to merge with the metadata.
     */
    private function createMetadata(array $route, $data = null)
    {
        $defaults = array(
            'pattern'  => '',
            'method'   => 'get',
            'callable' => array($this->getApp(), 'defaultNotFound')
        );
        $metadata = array_merge($defaults, (array) $data, $route);
        
        $callable = (isset($route['callable'])) ? $route['callable'] : null;
        if (is_string($callable) && preg_match('#^([^\:]+)\:([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)$#', $callable, $matches)) {            
            $metadata['callable'] = array(new $matches[1](), $matches[2]);
        } else if (is_array($callable)) {
            $metadata['callable'] = array_slice($callable, 0, 2); 
        } else {
            $metadata['callable'] = $callable;
        }
        
        return $metadata;
    }
    
    /**
     * Creates a new Route object.
     *
     * @param string $pattern the pattern to match a request uri with.
     * @param callable $callable the method or function to invoke.
     * @param string $method the HTTP request method (get, post, delete, etc..)
     * @return Route a new route for the given arguments.
     * @see Slim::mapRoute($args)
     * @see \Slim\Route::setCallable($callable)
     */
    private function createRoute($pattern, $callable, $method = 'get')
    {        
        $route = call_user_func(array($this->getApp(), strtolower($method)), $pattern, $callable);
        $route->setMiddleware(array($this, 'onDispatch'));
        
        return $route;
    }
    
    /**
     * Returns a collection of routes.
     * 
     * @return SplObjectStorage a collection type that associates data with an object.
     */
    public function getRoutes()
    {
        if ($this->routes === null) {
            $this->routes = new SplObjectStorage();
        }
        
        return $this->routes;
    }
    
    /**
     * Returns true if the given route has any metadata.
     *
     * @param Route the route for which to determine if it has metadata.
     * @return bool true if the given route has metadata, false otherwise.
     */
    public function hasMetadata(Route $route)
    {
        return ($this->getRoutes()->contains($route));
    }
    
    /**
     * Returns the metadata associated with the given route.
     *
     * @param Route the route for which to retrieve metadata.
     * @return mixed|null the metadata associated with the given route, or null.
     */
    public function getMetadata(Route $route)
    {
        $metadatas = $this->getRoutes();
        return ($this->hasMetadata($route)) ? $metadatas[$route] : null;
    }

    /**
     * Called whenever a route is dispatched.
     *
     * @param Route $route the route that is being dispatched.
     * @see Route::dispatch()
     */
    public function onDispatch(Route $route)
    {    
        $dispatcher = $this->getEventDispatcher();
        $event = new RouteEvent($this, $route, $this->getMetadata($route));
    
        $routes = $this->getRoutes();
        if (isset($routes[$route])) {
            $event->setCallable(new CallableDescriptor($routes[$route]['callable']));
        }

        // notify listener(s) that a Slim route has been dispatched.
        $dispatcher->dispatch(RouteEvent::EVENT_ROUTE_DISPATCHED, $event);
    }
    
    /**
     * {@inheritDoc}
     */
    public function setEventDispatcher(EventDispatcherInterface $newDispatcher)
    {
        if ($newDispatcher === null) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects instance of EventDispatcherInterface; received a NULL literal',
                __METHOD__
            ));
        }
        
        $oldDispatcher = $this->dispatcher;
        if ($oldDispatcher === $newDispatcher) {
            return;
        }

        $this->dispatcher = $newDispatcher;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getEventDispatcher()
    {
        if ($this->dispatcher === null) {
            $this->setEventDispatcher(new EventDispatcher());
        }
    
        return $this->dispatcher;
    }
    
    
    /**
     * Set the Slim application.
     *
     * @param Slim $app the Slim application.
     * @throws InvalidArgumentException if the given argument is a NULL literal.
     */
    public function setApp(Slim $app)
    {
        if ($app === null) {
            throw new \InvalidArgumentException(sprintf(
                '%s: does not allow a NULL literal',
                __METHOD__
            ));
        }
        
        $this->app = $app;
    }
    
    /**
     * Returns the Slim application.
     *
     * @return Slim|null the Slim application, or 'null' if a slim application has not yet been instantiated.
     */
    private function getApp()
    {
        if ($this->app === null) {
            $this->app = Slim::getInstance();
        }

        return $this->app;
    }
}
