<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Router\Event;

use Framework\EventDispatcher\Event;
use Framework\Slim\Common\CallableDescriptor;
use Slim\Route;

/**
 * The ModuleEvent is an event that describes a specific route.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class RouteEvent extends Event
{
    /**
     * The name of the event.
     *
     * @var string
     */
    const EVENT_ROUTE_DISPATCHED = 'route_dispatched';

    /**
     * An object that describes a callable function.
     *
     * @var Callable
     */
    private $callable;
    
    /**
     * A route object.
     * 
     * @var Route
     */
    private $route;

    /**
     * Metadata associated with the route.
     *
     * @var mixed
     */
    private $metadata;
    
    /**
     * The event source which sent the event.
     *
     * @var object
     */
    private $source;
    
    /**
     * Construct a RouteEvent.
     *
     * @param object $source the event source which sent the event.
     * @param Route $route the route object.
     */
    public function __construct($source, Route $route, $metadata = null)
    {
        $this->setSource($source);
        $this->setRoute($route);
        $this->setMetadata($metadata);
    }
    
    /**
     * Set object that describes a callable function.
     *
     * @param CallableDescriptor $callable object that describes a callable function.
     */
    public function setCallable(CallableDescriptor $callable)
    {
        $this->callable = $callable;
    }
    
    /**
     * Returns if present the object that describes a callable function.
     *
     * The {@link RouteEvent::hasCallable()} method can be used to determine
     * whether this event has an object that describes a callable function.
     *
     * @return CallableDescriptor|null object that describes a callable function, or null.
     */
    public function getCallable()
    {
        return $this->callable;
    }
    
    /**
     * Returns true if this event contains a callable object.
     *
     * @return bool true if this event contains a callable object, false otherwise..
     */
    public function hasCallable()
    {
        return ($this->callable !== null);
    }
    
    /**
     * Set a route object.
     *
     * @param Route $route the route object.
     */
    public function setRoute(Route $route)
    {
        $this->route = $route;
    }
    
    /**
     * Returns the route object.
     *
     * @return Route the route object.
     */
    public function getRoute()
    {
        return $this->route;
    }
    
    /**
     * Set the source that sent the event.
     *
     * @param object $source the source that send the event.
     * @throws InvalidArgumentException if the given argument is not an object.
     */
    public function setSource($source)
    {
        if (!is_object($source)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects an object; received "%s"',
                __METHOD__,
                (is_object($source) ? get_class($source) : gettype($source))
            ));
        }
    
        $this->source = $source;
    }
    
    /**
     * Returns the source that sent the event.
     *
     * @return object an object that sent the event.
     */
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * Set the metadata associated with the route.
     *
     * @param mixed $metadata the metadata.
     */
    public function setMetadata($metadata = null)
    {
        $this->metadata = $metadata;
    }
    
    /**
     * Returns if present the metadata for the route.
     *
     * @return mixed|null the metadata if present, otherwise null.
     */
    public function getMetadata()
    {
        return $this->metadata;
    }
}
