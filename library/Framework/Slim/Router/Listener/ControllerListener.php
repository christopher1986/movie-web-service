<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Slim\Router\Listener;

use Framework\Slim\Router\RouteManager;
use Framework\Slim\Router\Event\RouteEvent;
use Framework\ServiceLocator\ServiceLocatorInterface;
use Framework\Slim\Controller\ControllerInterface;
use Framework\Util\Arrays;

/**
 * The ControllerListener is triggered after a route has been dispatched.
 *
 * After a route has been dispatched the Slim framework will invoke a callable function
 * that is associated with the route. The ControllerListener determines if this callable
 * function is a controller and if so will setup that controller.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class ControllerListener
{
    /**
     * A locator that contains and creates services.
     *
     * @var ServiceLocatorInterface
     */
    private $locator;    
    
    /**
     * Construct a ControllerListener.
     *
     * @param ServiceLocatorInterface $locator a service locator.
     */
    public function __construct(ServiceLocatorInterface $locator)
    {
        $this->locator = $locator;
    }

    /**
     * A listener that setup a controller. 
     *
     * @param RouteEvent $event an object that contains detailed information about the event.
     */
    public function onRouteDispatched(RouteEvent $event)
    {        
        $controller = null;
        
        $callable = $event->getCallable();
        if (($callable !== null) && $callable->isMethod()) {
            $object = $callable->getObject();
            if ($object instanceof ControllerInterface) {
                $controller = $object;
            }
        }

        if ($controller) {       
            // merge route params and metadata params.
            $params = $event->getRoute()->getParams();
            if (($metadata = $event->getMetadata()) !== null && isset($metadata['params'])) {
                $params = Arrays::addAll((array) $metadata['params'], $params);
            }
            
            $controller->setParams($params);
            $controller->setServiceLocator($this->locator);

            // call init method.
            if (method_exists($controller, 'init')) {
                $controller->init();
            }
        }
    }
}
