<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Collection;

use Countable;
use Iterator;

/**
 * An interface from which most collection types are derived. A collection represents a group of objects or values. Within the collection 
 * these objects or values are collectively known as it's elements. The CollectionInterface describes the minimal implementation for a 
 * collection type where subinterfaces such as the ListInterface define more specific behavior.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
interface CollectionInterface extends Countable, Iterator
{
    /**
     * Add the specified element to this collection if it not already present.
     *
     * @param mixed $element the element to add to this collection.
     * @return bool true if this collection did not already contain the specified element.
     */
    public function add($element);
    
    /**
     * Add to this collection all of the elements that are contained in the specified collection.
     *
     * @param array|\Traversable $elements collection containing elements to add to this collection.
     * @return bool true if the collection has changed, false otherwise.
     * @throws \InvalidArgumentException if the given argument is not an array of instance of Traversable.
     * @see CollectionInterface::add($element)
     */
    public function addAll($elements);
    
    /**
     * Removes all elements from this collection. The collection will be empty after this call returns.
     *
     * @return void
     */
    public function clear();
    
    /**
     * Returns true if this collection contains the specified element. More formally returns true only if this collection
     * contains an element $e such that ($e === $element).
     *
     * @param mixed $element the element whose presence will be tested.
     * @return bool true if this collection contains the specified element, false otherwise.
     */
    public function contains($element);
    
    /**
     * Returns true if this collection contains all elements contained in the specified collection.
     *
     * @param array|\Traversable $elements collection elements whose presence will be tested.
     * @return bool true if this collection contains all elements in the specified collection, false otherwise.
     * @see CollectionInterface::contains($element)
     */
    public function containsAll($elements);
    
    /**
     * Returns true if this collection is considered to be empty.
     *
     * @return bool true is this collection contains no elements, false otherwise.
     */
    public function isEmpty();
    
    /**
     * Removes the specified element from this collection if it is present. More formally removes an element $e
     * such that ($e === $element), if this collection contains such an element.
     *
     * @param mixed $element the element to remove from this collection.
     * @return mixed the element that was removed from the collection, or null if the element was not found.
     */
    public function remove($element);

    /**
     * Removes from this collection all of the elements that are contained in the specified collection.
     *
     * @param array|\Traversable $elements collection containing elements to remove from this collection.
     * @return bool true if the collection has changed, false otherwise.
     * @throws \InvalidArgumentException if the given argument is not an array of instance of Traversable.
     * @see CollectionInterface::remove($element)
     */
    public function removeAll($elements);
    
    /**
     * Retains only the elements in this collection that are contained in the specified collection. In other words,
     * remove from this collection all of it's elements that are not contained in the specified collection.
     *
     * @param array|\Traversable $elements collection containing element to be retained in this collection.
     * @return bool true if the collection has changed, false otherwise.
     * @throws \InvalidArgumentException if the given argument is not an array of instance of Traversable.
     */
    public function retainAll($elements);
    
    /**
     * Returns an array containing all elements in this collection. The caller is free to modify the returned
     * array since it has no reference to the actual elements contained by this collection.
     *
     * @return array an array containing all elements from this collection.
     */
    public function toArray();
}
