<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Config;

/**
 * The {@link ConfigInterface} interface describes a class that will contain base configuration details
 * of the web service, such a database connection information and the module directory.
 * 
 * @author Chris Harris
 * @version 0.0.9
 */
interface ConfigInterface extends \IteratorAggregate
{
    /**
     * Associate the specified value with the specified key in this configuration. If a value was already associated with the given
     * key it's mapping to the key will be replaced by the new value.
     *
     * @param int $key the key to be mapped with the given value.
     * @param mixed $value the value to add to this configuration.
     * @return mixed the previously associated value with the key, or null if this key had no mapping to a value.
     * @throws InvalidArgumentException if the given key is null.
     */
    public function put($key, $value);

    /**
     * Returns the value associated with the specified key, or the default value if no value exists for the given key.
     *
     * @param mixed $key key to which zero or more values are mapped.
     * @return mixed if present the value associated with the given key, otherwise the default value.
     */
    public function get($key, $default = null);

    /**
     * Removes the specified value from this configuration if it is present. More formally removes a value $v
     * such that ($v === $value), if this configuration contains such a value.
     *
     * @param mixed $key the value to remove from this configuration.
     * @return mixed the value that was removed from the configuration, or null if the value was not found.
     */    
    public function remove($key);
    
    /**
     * Returns true if this configuration contains the specified key. More formally returns true only if this configuration
     * contains a value $k such that ($k === $key).
     *
     * @param mixed $key the key whose presence will be tested.
     * @return bool true if the configuration contains the given key, false otherwise.
     */
    public function containsKey($key);
    
    /**
     * Returns true if the specified value is stored within the configuration. More formally returns true only if this
     * configuration contains a value $v such that ($v === $value).
     *
     * @param mixed $value the value whose presence will be tested.
     * @return bool true if the given value is stored within the configuration, false otherwise.
     */
    public function containsValue($value);
    
    /**
     * Removes all values set for the configuration. All configurations will be removed after this call returns.
     *
     * @return void
     */
    public function clear();

    /**
     * Returns a collection of keys contained by the configuration.
     *
     * @return CollectionInterface a collection of keys for the configuration.
     */    
    public function keySet();

    /**
     * Returns a list containing all the valuess contained by the configuration.
     *
     * @return ListInterface a list with values contained by the configuration.
     */    
    public function values();
    
    /**
     * Returns an iterator object to traverse over all values within this configuration.
     *
     * @return ArrayIterator an iterator object.
     */
    public function getIterator();
    
    /**
     * Merge the given config object with this config.
     *
     * @param ConfigInterface $config the config to merge.
     */
    public function merge(ConfigInterface $config);

    /**
     * Returns an array that represents this configuration object.
     *
     * @return array an array that represents this configuration object.
     */
    public function toArray();
}
