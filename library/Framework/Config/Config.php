<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Config;

use ArrayIterator;
use Countable;

/**
 * An object that stores configuration by mapping keys to values.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
class Config implements ConfigInterface, Countable
{
    /**
     * A native array to hold the values.
     *
     * @var array
     */
    public $data = array();

    /**
     * Construct a new Config.
     *
     * @param array|null $data the data to store within this config.
     */
    public function __construct(array $data = null)
    {
        if ($data !== null) {
            foreach ($data as $key => $value) {
                $this->put($key, $value);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function put($key, $value)
    {
        if ($key === null) {
            throw new \InvalidArgumentException(sprintf(
                '%s: does not allow a key to be NULL; received "%s"',
                __METHOD__,
                (is_object($key) ? get_class($key) : gettype($key))
            ));
        }

        $retval = $this->get($key);
        if ($retval !== $value) {
            if (is_array($value)) {
                $this->data[$key] = new static($value);
            } else {
                $this->data[$key] = $value;
            }
        }
        return $retval;
    }

    /**
     * {@inheritDoc}
     */
    public function get($key, $default = null)
    {
        return ($this->containsKey($key)) ? $this->data[$key] : $default;
    }

    /**
     * {@inheritDoc}
     */   
    public function remove($key)
    {
        $retval = null;
        if ($this->containsKey($key)) {
            $retval = $this->get($key);
            unset($this->data[$key]);
        }
        
        return $retval;
    }
    
    /**
     * {@inheritDoc}
     */
    public function containsKey($key)
    {
        return (isset($this->data[$key]));
    }
    
    /**
     * {@inheritDoc}
     */
    public function containsValue($value)
    {
        return (in_array($value, $this->data));
    }
    
    /**
     * {@inheritDoc}
     */
    public function clear()
    {
        $this->data = array();
    }

    /**
     * {@inheritDoc}
     */   
    public function keySet()
    {
        return new Set(array_keys($this->data));
    }

    /**
     * {@inheritDoc}
     */ 
    public function values()
    {
        return new ArrayList(array_values($this->data));
    }
    
    /**
     * {@inheritDoc}
     */
    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }
    
    /**
     * {@inheritDoc}
     */
    public function merge(ConfigInterface $config)
    {
        foreach ($config as $key => $value) {
            if ($this->containsKey($key)) {
                $originalValue = $this->get($key);
                if ($originalValue instanceof self && $value instanceof self) {
                    $originalValue->merge($value);
                } else {
                    if ($originalValue instanceof self) {
                        $originalValue->put($key, $value);
                    } else {
                        $this->put($key, $value);
                    }
                }
            } else {
                $this->put($key, $value);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public function toArray()
    {
        $retval = array();
        
        $data = $this->data;
        foreach ($data as $key => $value)
        {
            if ($value instanceof self) {
                $retval[$key] = $value->toArray();
            } else {
                $retval[$key] = $value;
            }
        }
        
        return $retval;
    }
    
    /**
     * Returns the number of values contained by this config.
     *
     * @return int the number of values contained by this config.
     */
    public function count()
    {
        return (count($this->data));
    }
    
    /**
     * Returns if present the value from this config associated with the given property name.
     *
     * @param string $name the property name.
     * @return mixed|null the value associated with the given property name, otherwise null.
     * @see Config::get($key, $default)
     * @link http://php.net/manual/en/language.oop5.overloading.php#object.get
     */
     public function __get($name)
     {
        return $this->get($name);
     }
     
     /**
      * Store the value in this config associated with the given property name.
      *
      * @param string $name the property name.
      * @param mixed $value the value to store.
      * @see Config::put($key, $value)
      * @link http://php.net/manual/en/language.oop5.overloading.php#object.set
      */
     public function __set($name, $value)
     {
        $this->put($name, $value);
     }
}
