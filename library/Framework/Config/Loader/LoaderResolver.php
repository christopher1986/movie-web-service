<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Config\Loader;

use Framework\Collection\Set;

/**
 * The LoaderResolver contains a collection of unique loaders. A loader is capable of loading one or more resource types.
 * The {@link LoaderInterface::accept($resource)} method can be used to determine whether a loader is capable of handling 
 * a resource. A LoaderResolver will call this method on each loader until a loader is found that can handle the resource.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
class LoaderResolver implements LoaderResolverInterface
{
    /**
     * A collection of loaders.
     *
     * @var Set
     */
    private $loaders;

    /**
     * Construct a new LoaderResolver.
     *
     * @param array|Traversable|null $loaders a collection of loaders to add.
     */
    public function __construct($loaders = null)
    {
        $this->loaders = new Set();
        
        if ($loaders !== null) {
            $this->addLoaders($loaders);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function resolve($resource)
    {
        $retval = null;
        if (!$this->loaders->isEmpty()) {
            foreach ($this->loaders as $loader) {
                if ($loader->accepts($resource)) {
                    $retval = $loader;
                    break;
                }
            }
        }
        
        return $retval;
    }
    
    /**
     * Add a collection of loaders.
     *
     * @param array|Traversable $loaders a collection of loaders to add.
     * @return bool true if a loader from the given collection was added, false otherwise.
     * @throws InvalidArgumentException if the given argument if not an array or traversable object.
     */
    public function addLoaders($loaders)
    {
        if (!is_array($loaders) && !($loaders instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects an array or instance of the Traversable; received "%s"',
                __METHOD__,
                (is_object($loaders) ? get_class($loaders) : gettype($loaders))
            ));
        }
        
        $modified = false;
        foreach ($loaders as $loader) {
            if ($this->addLoader($loader)) {        
                $modified = true;
            }
        }
        return $modified;
    }
    
    /**
     * Add the given loader.
     *
     * @param LoaderInterface $loader the loader to add.
     * @return bool true if the loader was added, false otherwise.
     */
    public function addLoader(LoaderInterface $loader)
    {
        return $this->loaders->add($loader);
    }
    
    /**
     * Determines whether the loader is known to the resolver.
     *
     * @param LoaderInterface $loader the loader whose presence will be tested.
     * @retunr bool true if the loader is already known to the resolver, false otherwise.
     */
    public function hasLoader(LoaderInterface $loader)
    {
        return $this->loaders->contains($loader);
    }
    
    /**
     * Removes if present the given loader.
     *
     * @param LoaderInterface $loader the loader to remove.
     * @return bool true if the loader was removed, false otherwise.
     */
    public function removeLoader(LoaderInterface $loader)
    {
        return $this->loaders->remove($loader);
    }
    
    /**
     * Removes all loaders known to the resolver.
     * 
     * @return void
     */
    public function clearLoaders()
    {
        $this->loaders->clear();
    }
    
    /**
     * Returns an array of loaders.
     *
     * @return array array of loaders.
     */
    private function getLoaders()
    {
        return $this->loaders->toArray();
    }
}
