<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Config\Loader;

use SplFileInfo;
use XMLReader;

use Framework\Config\Exception\ConfigurationException;
use Framework\Io\Exception\AccessDeniedException;
use Framework\Util\Strings;

class XmlFileLoader implements LoaderInterface
{
    /**
     * {@inheritDoc}
     *
     * @param string $file the XML file to load.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     * @throws FileNotFoundException if the given file does not exist.
     * @throws AccessDeniedException if the given file has no read permission.
     * @throws ConfigurationException if the root node is not named 'config'.
     */
    public function load($file)
    {
	    if (!is_string($file)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($file) ? get_class($file) : gettype($file))
            ));
	    } else if (!is_file($file)) {
            throw new FileNotFoundException($file, 'file does not exist, or symbolic link is pointing to non-existing file.');
	    } else if (!is_readable($file)) {
	        throw new AccessDeniedException($file, 'unable to read file, make sure the file has read permissions');
	    }
	    
	    return $this->parseXmlFile($file);
    }
    
    /**
     * {@inheritDoc}
     */
    public function accepts($resource)
    {
        if (is_string($resource)) {
            $info = new SplFileInfo($resource);
            return ($info->isFile() && $info->getExtension() === 'xml');
        }
        return false;
    }
    
    /**
     * Parses the given XML file into an associative array.
     *
     * @param string $file the XML file to parse.
     * @return array an associative array.
     * @see XmlFileLoader::xmlToArray($reader)
     */
    private function parseXmlFile($file)
    {
        $retval = array();
        $reader = new XmlReader();
	    if ($reader->open($file)) {
            $retval = $this->xmlToArray($reader);
        }
        
        return $retval;
    }
    
    /**
     * Returns an associative array from the XML for which the given reader was created.
     *
     * @param XMLReader $reader the reader containing the XML structure to read.
     * @return array an associative array.
     */
    private function xmlToArray(XMLReader $reader)
    {
        // a boolean flag to stop reading.
        $stopReading = false;
        
        $children = array();
        $text = '';
        
        while($reader->read() && !$stopReading) {
            if ($reader->depth === 0) {
                continue;
            }
        
            switch($reader->nodeType) {
                case XMLReader::ELEMENT:
                    $name = $this->normalizeName($reader->name);
                    if ($reader->isEmptyElement) {
                        $child = array();
                    } else {
                        $child = $this->xmlToArray($reader);
                    }
                                        
                    if (isset($children[$name])) {
                        if (!is_array($children[$name]) || !array_key_exists(0, $children[$name])) {
                            $children[$name] = array($children[$name]);
                        }
                        $children[$name][] = $child;
                    } else {
                        $children[$name] = $child;
                    }
                    
                    break;
                case XMLReader::TEXT:
                case XMLReader::CDATA:
                case XMLReader::WHITESPACE:
                case XMLReader::SIGNIFICANT_WHITESPACE:
                    $text .= $reader->value;
                    break;
                case XMLReader::END_ELEMENT:
                    $stopReading = true;
                    break;
            }
        }
        
        return (!empty($children)) ? $children : $text;
    }
    
    /**
     * Returns a normalized key from the given node name.
     *
     * This method normalizes all camel case element names to underscore case. So for example 
     * an element with the following name 'dbName' will be normalized into 'db_name'.
     *
     * @param string $name the node name to normalize.
     * @return string a normalized name.
     */
    private function normalizeName($name)
    {
        $lowercase = function($letters) {
            $letter = array_shift($letters);
            return '_' . strtolower($letter);
        };
        
        return preg_replace_callback('#([A-Z])#', $lowercase, Strings::lcfirst(trim($name)));
    }
}
