<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\Config\Loader;

use SplFileInfo;

use Framework\Io\Exception\AccessDeniedException;

class PhpFileLoader implements LoaderInterface
{
    /**
     * {@inheritDoc}
     *
     * @param string $file the PHP file to load.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     * @throws FileNotFoundException if the given file does not exist.
     * @throws AccessDeniedException if the given file has no read permission.
     */
    public function load($file)
    {
	    if (!is_string($file)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($file) ? get_class($file) : gettype($file))
            ));
	    } else if (!is_file($file)) {
            throw new FileNotFoundException($file, 'file does not exist, or symbolic link is pointing to non-existing file.');
	    } else if (!is_readable($file)) {
	        throw new AccessDeniedException($file, 'unable to read file, make sure the file has read permissions');
	    }
	    
        return require($file);
    }
    
    /**
     * {@inheritDoc}
     */
    public function accepts($resource)
    {
        if (is_string($resource)) {
            $info = new SplFileInfo($resource);
            return ($info->isFile() && $info->getExtension() === 'php');
        }
        return false;
    }
}
