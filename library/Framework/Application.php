<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework;

use Framework\Config\Config;
use Framework\Config\Configurable;
use Framework\Config\Loader\LoaderResolver;
use Framework\Config\Loader\PhpFileLoader;
use Framework\Config\Loader\XmlFileLoader;
use Framework\EventDispatcher\EventDispatcher;
use Framework\EventDispatcher\EventDispatcherAwareInterface;
use Framework\Loader\AutoloaderFactory;
use Framework\ModuleManager\Event\ModuleEvent;
use Framework\ModuleManager\Listener\AutoloaderListener;
use Framework\ModuleManager\Listener\ConfigListener;
use Framework\ModuleManager\Listener\ServiceListener;
use Framework\ModuleManager\ModuleManager;
use Framework\ModuleManager\ModuleConfig;
use Framework\ServiceLocator\ServiceLocator;
use Framework\Slim\Router\RouteManager;
use Framework\Slim\Router\Event\RouteEvent;
use Framework\Slim\Router\Listener\ControllerListener;
use Framework\Util\Strings;
use Framework\Util\Arrays;

use Slim\Slim;

use ReflectionClass;

/**
 * The application class is responsible for initializing the application.
 *
 * @author Chris Harris <c.harris@hotmail.com>
 * @version 1.0.0
 */
abstract class Application implements Configurable
{
    /**
     * A locator that contains and creates services.
     *
     * @var ServiceLocatorInterface
     */
    private $locator;

    /**
     * Create a new Application.
     *
     * @return void
     */
    public function __construct()
    {       
        $this->registerAutoloader();
        $this->registerServices();
        
        // call parameterless methods that start with '_init' prefix.
        $methods = $this->getClassMethods($this);
        foreach ($methods as $method) {
            if ($method->getNumberOfParameters() == 0 && strpos($method->getName(), '_init') === 0) {
                $method->setAccessible(true);
                $method->invoke($this);
            }
        }

        $this->setupModules();
    }
    
    /**
     * Run the application.
     *
     * @return self 
     * @see Slim::run()
     */
    public function run()
    {
        $this->getServiceLocator()->get('Slim')->run();
        return $this;
    }
    
    /**
     * Add listener(s) that are triggered for module events.
     *
     * @return void
     */
    private function _initEvents()
    {        
        $locator = $this->getServiceLocator();
        
        $dispatcher = $locator->get('EventDispatcher');
        $dispatcher->addEventHandler(ModuleEvent::EVENT_MODULE_LOADED, array(new AutoloaderListener(), 'onModuleLoaded'))
                   ->addEventHandler(ModuleEvent::EVENT_MODULE_LOADED, array(new ConfigListener(), 'onModuleLoaded'))
                   ->addEventHandler(ModuleEvent::EVENT_MODULE_LOADED, array(new ServiceListener($locator), 'onModuleLoaded'), 100)
                   ->addEventHandler(RouteEvent::EVENT_ROUTE_DISPATCHED, array(new ControllerListener($locator), 'onRouteDispatched'));
    }
    
    /**
     * Register additional middleware with Slim.
     *
     * @return void
     */
    private function _initMiddleware()
    {
        $slim = $this->getServiceLocator()->get('Slim');
        $slim->add(new \Framework\Slim\Middleware\ResponseMiddleware());
    }
        
    /**
     * Load modules from a configuration file.
     *
     * @return void
     */
    private function setupModules()
    {
        $config  = $this->getServiceLocator()->get('Config');
        $moduleManager = $this->getServiceLocator()->get('ModuleManager');
    
        $file = $config->get('config')->get('modules', './app/config/modules.php');
        if (!Strings::startsWith($file, DIRECTORY_SEPARATOR)) {
            $file = realpath($file);
        }

        $modules = array();
        if (($loader = $this->getConfigLoader($file)) !== null) {
            $config = $loader->load($file);
            if (isset($config['module']) && is_array($config['module'])) {
                $moduleConfig = (Arrays::isAssoc($config['module'])) ? array($config['module']) : $config['module'];
                foreach ($moduleConfig as $settings) {
                    $settings = array_merge(array(
                        'name'     => '', 
                        'active'   => false,
                        'priority' => -1
                    ), (array) $settings);
                    
                    $modules[] = new ModuleConfig($settings['name'], $settings['active'], $settings['priority']);
                }
            }
        }     

        $moduleManager->loadModules($modules);        
    }
    
    /**
     * Register services with the service manager.
     *
     * Services can be directly registered with the manager or be created by the manager through
     * the use of a fully qualified class name (invokable) or a factory whose sole task is to
     * create and return the service.
     *
     * @return void
     */
    private function registerServices()
    {
        $locator = $this->getServiceLocator();
        
        // application service.
        $locator->service('App', $this);
        // event dispatcher service.
        $locator->invokable('EventDispatcher', 'Framework\EventDispatcher\EventDispatcher');
        // config factory service.
        $locator->factory('Config', function($locator) {
            $file = $locator->get('App')->registerConfiguration(); 
            $config = new Config();
            if ($loader = $locator->get('App')->getConfigLoader($file)) {
                foreach ($loader->load($file) as $key => $value) {
                    $config->put($key, $value);
                }
            }
            
            return $config;
        }); 
        // module manager factory service.
        $locator->factory('ModuleManager', function($locator) {
            $moduleManager = new ModuleManager($locator->get('Config'));
            if ($locator->has('EventDispatcher')) {
                $moduleManager->setEventDispatcher($locator->get('EventDispatcher'));
            }
            
            return $moduleManager;
        });
        // slim factory service.
        $locator->factory('Slim', function($locator) {
            $file = $locator->get('Config')->get('config')->get('slim', './app/config/slim.php');
            if (!Strings::startsWith($file, DIRECTORY_SEPARATOR)) {
                $file = realpath($file);
            }
            
            $settings = array();
            if ($loader = $locator->get('App')->getConfigLoader($file)) {
                $settings = $loader->load($file);
            }
            
            return new Slim($settings);
        });
        // route manager factory service.
        $locator->factory('RouteManager', function($locator) {
            $routeManager = new RouteManager($locator->get('Slim'));
            if ($locator->has('EventDispatcher')) {
                $routeManager->setEventDispatcher($locator->get('EventDispatcher'));
            }
            
            return $routeManager;
        });
    }
    
    /** 
     * Initialize autoloaders and register the 'Framework' namespace.
     *
     * @return void
     */
    private function registerAutoloader()
    {
        require_once __DIR__ . '/Loader/AutoloaderFactory.php';

        AutoloaderFactory::factory(array(
            'Framework\Loader\StandardAutoloader' => array(
                'autoregister_framework' => true
            ),
        ));
    }
    
    /**
     * Returns a config loader that is capable of loading the given resource. 
     *
     * @param string $resource the resource for which to retrieve a loader.
     * @return LoaderInterface|null a loader for the given resource, or null if no loader can load the resource.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     */
    public function getConfigLoader($resource)
    {
        if (!is_string($resource)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($resource) ? get_class($resource) : gettype($resource))
            ));
        }

        $resolver = new LoaderResolver(array(
            new PhpFileLoader(),
            new XmlFileLoader()    
        ));
        
        return $resolver->resolve($resource);
    }
    
    /**
     * Returns service locator to register services with. 
     *
     * A new service locator will be lazy initialized if one has not yet been instantiated.
     *
     * @return ServiceLocator a service locator.
     */
    protected function getServiceLocator()
    {
        if ($this->locator === null) {
            $this->locator = new ServiceLocator();
        }
        
        return $this->locator;
    }
    
    /**
     * Returns an array containing {@link Reflectionmethod}s for the given class.
     * 
     * The {@link ReflectionMethod}s are stored in such a way that methods of parent classes are stored 
     * before methods of their child class. If methods of the child class should come first use the 
     * {@link ReflectionClass::getMethods($filter)} method instead.
     *
     * @param string|object $class the class to return methods for.
     * @return array returns an array containing zero or more reflection methods.
     * @link http://php.net/manual/en/reflectionclass.getparentclass.php
     * @link http://php.net/manual/en/reflectionclass.getmethods.php
     */
    private function getClassMethods($class) 
    {
        // sorted methods.
        $queue = array();
        
        $class = new ReflectionClass($class);
        do {
            $stack = array();
            foreach ($class->getMethods() as $method) {
                // only store methods belonging to this class.
                if (!$method->isAbstract() && $method->getDeclaringClass() == $class) {
                    $stack[] = $method;
                }
            }
            // add methods to queue.
            $queue = array_merge($stack, $queue);
        } while ($class = $class->getParentClass());
        
        return $queue;
    } 
}
