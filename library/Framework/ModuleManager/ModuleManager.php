<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\ModuleManager;

use Framework\Collection\HashSet;
use Framework\Collection\PriorityList;
use Framework\Config\ConfigAwareInterface;
use Framework\Config\ConfigInterface;
use Framework\EventDispatcher\EventDispatcher;
use Framework\EventDispatcher\EventDispatcherInterface;
use Framework\EventDispatcher\EventDispatcherAwareInterface;
use Framework\ModuleManager\Event\ModuleEvent;
use Framework\ModuleManager\Resolver\ModuleResolvable;
use Framework\ModuleManager\Resolver\ModuleResolver;
use Framework\Util\Arrays;
use Framework\Util\Strings;

/**
 * The ModuleManager is capable of loading a module through a {@link ModuleConfig} object.
 *
 * A ModuleConfig contains the settings of a single module and is used by the manager to
 * resolve and instantiate a module. A module can only be loaded once through the manager
 * and providing the same module a second time is considered a no-op.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class ModuleManager implements EventDispatcherAwareInterface, ConfigAwareInterface
{    
    /**
     * Configuration data for the application.
     *
     * @var ConfigInterface
     */
    private $config;

    /**
     * An object to notify event listeners.
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * An object that resolves the path to a module.
     *
     * @var ModuleResolvable
     */
    private $moduleResolver;
    
    /**
     * A set of loaded modules.
     *
     * @var HashSet
     */
    private static $loaded;
    
    /**
     * Construct a new ModuleManager.
     *
     * @param ConfigInterface $config configuration data for the application.
     */
    public function __construct(ConfigInterface $config)
    {
        $this->setConfig($config);
    }
    
    /**
     * Load the given module.
     *
     * @param ModuleConfig $moduleConfig a module config object.
     */
    public function loadModule(ModuleConfig $moduleConfig)
    {
        $this->loadModules(array($moduleConfig));
    }
    
    /**
     * Load a collection of modules.
     *
     * @param array|Traversable $moduleConfigs a collection of module configs.
     * @throws InvalidArgumentException if the given argument is not an array or Traversable object.
     * @see ModuleManager::loadModule($module)
     */
    public function loadModules($moduleConfigs)
    {
        if (!is_array($moduleConfigs) && !($moduleConfigs instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects an array or instance of the Traversable; received "%s"',
                __METHOD__,
                (is_object($moduleConfigs) ? get_class($moduleConfigs) : gettype($moduleConfigs))
            ));
        }
        
        $resolver = $this->getModuleResolver();
        if ($resolver) {
            // notify listeners about new modules.
            $dispatcher = $this->getEventDispatcher();
            // a priority list of modules to load.               
            $moduleConfigs = new PriorityList($moduleConfigs);
            foreach ($moduleConfigs as $moduleConfig) {
                // determine is module is loadable.
                if (!$this->isLoadable($moduleConfig)) {
                    continue;
                }
                
                // try to resolve module.
                if (($module = $resolver->resolve($moduleConfig)) !== null) {
                    $this->getLoadedModules()->add($moduleConfig);
                    // notify listener(s) that a module has been loaded.
                    $dispatcher->dispatch(ModuleEvent::EVENT_MODULE_LOADED, new ModuleEvent($this, $module));
                }
            }
            
            // finished loading modules.
            $moduleConfigs->clear();
        }
    }
    
    /**
     * Returns true if the given module can be loaded.
     *
     * @param mixed $moduleConfig the module to be tested.
     * @return bool true if the module is loadable, false otherwise.
     */
    private function isLoadable($moduleConfig)
    {
        // a set of loaded modules.
        $loaded = $this->getLoadedModules();
        
        $retval = true;
        if (!($moduleConfig instanceof ModuleConfig)) {
            $retval = false;
        } else if (!$moduleConfig->isActive()) {
            $retval = false;
        } else if ($loaded->contains($moduleConfig)) {
            $retval = false;
        }
        
        return $retval; 
    }
    
    /**
     * Set a module resolver.
     *
     * @param ModuleResolvable $resolver a module resolver.
     */
    public function setModuleResolver(ModuleResolvable $resolver)
    {
        $this->moduleResolver = $resolver;
    }
    
    /**
     * Returns the module resolver.
     *
     * @return ModuleResolvable a module resolver.
     */
    public function getModuleResolver()
    {
        if ($this->moduleResolver === null) {
            $this->moduleResolver = new ModuleResolver($this->getConfig());
        }
        
        return $this->moduleResolver;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setConfig(ConfigInterface $config)
    {
        if ($config === null) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects instance of ConfigInterface; received a NULL literal',
                __METHOD__
            ));
        }
        
        $this->config = $config;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return $this->config;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setEventDispatcher(EventDispatcherInterface $newDispatcher)
    {
        if ($newDispatcher === null) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects instance of EventDispatcherInterface; received a NULL literal',
                __METHOD__
            ));
        }
        
        $oldDispatcher = $this->dispatcher;
        if ($oldDispatcher === $newDispatcher) {
            return;
        }

        $this->dispatcher = $newDispatcher;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getEventDispatcher()
    {
        if ($this->dispatcher === null) {
            $this->setEventDispatcher(new EventDispatcher());
        }
    
        return $this->dispatcher;
    }
    
    /**
     * Returns a collection of modules that been loaded.
     *
     * @return Set
     */
    public function getLoadedModules()
    {
        if (static::$loaded === null) {
            static::$loaded = new HashSet();
        }
        
        return static::$loaded;
    }
}
