<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\ModuleManager\Event;

use Framework\Config\ConfigInterface;
use Framework\EventDispatcher\Event;

/**
 * The ModuleEvent is an event that describes a specific module.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class ModuleEvent extends Event
{
    /**
     * The name of the event.
     *
     * @var string
     */
    const EVENT_MODULE_LOADED = 'module_loaded';

    /**
     * A module object.
     *
     * @var ModuleInterface|object
     */
    private $module;
    
    /**
     * The event source which sent the event.
     *
     * @var object
     */
    private $source;

    /**
     * A configuration object.
     *
     * @var ConfigInterface
     */
    private $config;

    /**
     * Construct a ModuleEvent.
     *
     * @param object $source the event source which sent the event.
     * @param ModuleInterface|object $module the module object.
     */
    public function __construct($source, $module)
    {
        $this->setSource($source);
        $this->setModule($module);
    }
    
    /**
     * Set a module object.
     *
     *
     * @param ModuleInterface|object $module the module object.
     */
    public function setModule($module)
    {
        $this->module = $module;
    }
    
    /**
     * Returns the module object.
     *
     * @return ModuleInterface|object the module object.
     */
    public function getModule()
    {
        return $this->module;
    }
    
    /**
     * Set the source that sent the event.
     *
     * @param object $source the source that send the event.
     * @throws InvalidArgumentException if the given argument is not an object.
     */
    public function setSource($source)
    {
        if (!is_object($source)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects an object; received "%s"',
                __METHOD__,
                (is_object($source) ? get_class($source) : gettype($source))
            ));
        }
    
        $this->source = $source;
    }
    
    /**
     * Returns the source that sent the event.
     *
     * @return object an object that sent the event.
     */
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * Set a configuration object for the module.
     *
     * @param ConfigInterface $config a configuration object.
     */
    public function setConfig(ConfigInterface $config)
    {
        $this->config = $config;
    }
    
    /**
     * Returns if present a configuration object.
     *
     * @return ConfigInterface|null configuration object, or null if the module is not configurable.
     */
    public function getConfig()
    {
        return $this->config;
    }
}
