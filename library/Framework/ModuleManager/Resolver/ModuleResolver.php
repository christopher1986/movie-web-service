<?php 
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\ModuleManager\Resolver;

use Framework\Config\ConfigAwareInterface;
use Framework\Config\ConfigInterface;
use Framework\ModuleManager\ModuleConfig;
use Framework\Util\Strings;

/**
 * A class that is capable of resolving the path to a module.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class ModuleResolver implements ModuleResolvable, ConfigAwareInterface
{

    /**
     * Configuration data for the application.
     *
     * @var ConfigInterface
     */
    private $config;

    /**
     * A collection of modules that have been resolved.
     *
     * @var array
     */
    private static $resolved = array();

    /**
     * Construct a new ModuleResolver.
     *
     * @param ConfigInterface $config configuration data for the application.
     */
    public function __construct(ConfigInterface $config)
    {
        $this->setConfig($config);
    }

    /**
     * Returns an instantiated Module object, or null if the module could not be resolved.
     *
     * @param ModuleConfig $module the module whose path to resolve.
     * @return object|null an instantiated Module object, or null on failure.
     */
    public function resolve(ModuleConfig $module)
    {
        $class = null;
        $moduleName = $module->getName();
        
        // check if module has been resolved before.
        if (isset(self::$resolved[$moduleName])) {
            $className = self::$resolved[$moduleName];
            if ($className !== null) {
                return new $className();
            }
        }
        
        // load module for the first time.
        $this->loadModule($moduleName);
        
        $className = sprintf('%s\Module', $moduleName);
        if (class_exists($className)) {
            // optimize this resolve method.
            self::$resolved[$moduleName] = $className;
        
            $class = new $className();
        }
                
        return $class;    
    }
    
    /**
     * {@inheritDoc}
     */
    public function setConfig(ConfigInterface $config)
    {
        if ($config === null) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects instance of ConfigInterface; received a NULL literal',
                __METHOD__
            ));
        }
        
        $this->config = $config;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return $this->config;
    }
    
    
    /**
     * Load the given module so it can instantiated.
     *
     * @param $moduleName the name of the module load.
     */
    private function loadModule($moduleName)
    {
        $paths = $this->getConfig()->get('paths');
        $getModuleDirectory = function($moduleName) use ($paths) {
            $directory = null;
            if ($paths instanceof ConfigInterface) {
                $directory = Strings::addTrailing($paths->get('module', ''), DIRECTORY_SEPARATOR) . $moduleName;
            }
            
            return $directory;
        };
        
        if (($directory = $getModuleDirectory($moduleName)) !== null) {
            $filename = $directory . DIRECTORY_SEPARATOR . 'Module.php';
            if (file_exists($filename)) {
                require_once $filename;
            }
        }
    }
}
