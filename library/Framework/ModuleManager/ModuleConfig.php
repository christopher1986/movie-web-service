<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\ModuleManager;

use Framework\Common\Comparable;
use Framework\Common\Hashable;

class ModuleConfig implements Comparable, Hashable
{
    /**
     * The module name.
     *
     * @var string
     */
    private $name;
    
    /**
     * The module status.
     *
     * @var boolean
     */
    private $active;
    
    /**
     * The module priority.
     *
     * @var int
     */
    private $priority;
    
    /**
     * Constructs a module.
     *
     * @param string $name the name of the module.
     * @param bool $active the status of the module.
     */
    public function __construct($name = '', $active = false, $priority = -1)
    {        
        $this->setName($name);
        $this->setActive($active);
        $this->setPriority($priority);
    }
    
    /**
     * Set the name of the module.
     *
     * @param string $name the name of the module.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     */
    public function setName($name)
    {
	    if (!is_string($name)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($name) ? get_class($name) : gettype($name))
            ));
	    }
	    
	    $this->name = $name;
    }
    
    /**
     * Returns the name of the module.
     *
     * @return string the name of the module.
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set the status of the module.
     *
     * @param bool $active the status of the module.
     */
    public function setActive($active)
    {
        $this->active = (bool) $active;
    }
    
    /**
     * Returns true if the module is active, false otherwise.
     *
     * @return bool true if the module active, false otherwise.
     */
    public function isActive()
    {
        return $this->active;
    }
    
    /**
     * Set the priority of the module.
     *
     * Only positive priorities affect the priority of a module. Providing
     * this method with a negative priority removes the previous priority.
     *
     * @param int $priority 
     */
    public function setPriority($priority)
    {
        if (!is_numeric($priority)) {
            throw new InvalidArgumentException(sprintf(
                '%s: expects a numeric argument; received "%s"',
                __METHOD__,
                (is_object($priority) ? get_class($priority) : gettype($priority))
            ));
        }
        
        $this->priority = (int) $priority;
    }
    
    /**
     * Returns the priority of this module. A negative priority means that the module has 
     * no priority set.
     *
     * @return int the priority of the module.
     * @see ModuleConfig::setPriority($priority)
     */
    public function getPriority()
    {
        return $this->priority;   
    }
    
    /**
     * {@inherDoc}
     */
    public function compareTo($obj)
    {
        if ($this->getPriority() >= 0 && $obj instanceof self) {
            if ($obj->getPriority() < 0 || $obj->getPriority() > $this->getPriority()) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }
    
    /**
     * {@inheritDoc}
     *
     * @link http://php.net/manual/en/function.hash.php hash
     */
    public function hashCode()
    {
        return hash('md5', $this->getName());
    }
}
