<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\ModuleManager\Listener;

use Framework\Config\Config;
use Framework\Config\Configurable;
use Framework\Config\Loader\LoaderResolver;
use Framework\Config\Loader\PhpFileLoader;
use Framework\Config\Loader\XmlFileLoader;
use Framework\ModuleManager\Event\ModuleEvent;

/**
 * The ConfigListener will load if present the configuration file for a module.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class ConfigListener
{
    /**
     * A listener that will retrieve if present the path to a configuration file. 
     *
     * @param ModuleEvent $event an object that contains detailed information about the event.
     */
    public function onModuleLoaded(ModuleEvent $event)
    {    
        $module =$event->getModule();
        if ($module instanceof Configurable) {
            $file = $module->registerConfiguration();
            
            $config = new Config();
            if ($loader = $this->getConfigLoader($file)) {
                foreach ($loader->load($file) as $key => $value) {
                    $config->put($key, $value);
                }
            }
            
            // add Config to event for other listeners.
            $event->setConfig($config);
        }
    }
    
    /**
     * Returns a config loader that is capable of loading the given resource. 
     *
     * @param string $resource the resource for which to retrieve a loader.
     * @return LoaderInterface|null a loader for the given resource, or null if no loader can load the resource.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     */
    public function getConfigLoader($resource)
    {
        if (!is_string($resource)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($resource) ? get_class($resource) : gettype($resource))
            ));
        }

        $resolver = new LoaderResolver(array(
            new PhpFileLoader(),
            new XmlFileLoader()    
        ));
        
        return $resolver->resolve($resource);
    }
}
