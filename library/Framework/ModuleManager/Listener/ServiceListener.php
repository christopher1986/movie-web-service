<?php
/**
 * Copyright (c) 2015, Chris Harris.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of the copyright holder nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author     Chris Harris <c.harris@hotmail.com>
 * @copyright  Copyright (c) 2015 Chris Harris
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 */

namespace Framework\ModuleManager\Listener;

use Framework\Config\Configurable;
use Framework\ModuleManager\Event\ModuleEvent;
use Framework\ServiceLocator\ServiceLocatorAwareInteface;
use Framework\ServiceLocator\ServiceLocatorInterface;

/**
 * The ServiceListener will add a service locator to a loaded module.
 *
 * @author Chris Harris
 * @version 1.0.0
 */
class ServiceListener 
{
    /**
     * A locator that contains and creates services.
     *
     * @var ServiceLocatorInterface
     */
    private $locator;    
    
    /**
     * Construct a ModuleListener.
     *
     * @param ServiceLocatorInterface $locator a service locator.
     */
    public function __construct(ServiceLocatorInterface $locator)
    {
        $this->locator = $locator;
    }

    /**
     * A listener that will add a service locator to the module. 
     *
     * @param ModuleEvent $event an object that contains detailed information about the event.
     */
    public function onModuleLoaded(ModuleEvent $event)
    {
        $module =$event->getModule();
        if ($module instanceof ServiceLocatorAwareInteface) {
            $module->setServiceLocator($this->locator);
        }
        
        if (($config = $event->getConfig()) !== null) {
            $routeManager = $this->locator->get('RouteManager');
            if (($routes = $config->get('routes')) !== null) {
                $routeManager->factory($routes->toArray());
            }
        }
    }
}
